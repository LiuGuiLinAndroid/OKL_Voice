package com.okl.okl_voice.service;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   TimeService
 *  创建者:   LGL
 *  创建时间:  2016/8/30 14:42
 *  描述：    时间校准服务
 */

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.view.SessionLinearLayout;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeService extends Service {

    //时间格式，精确到秒就好了
    SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日 hh");
    //窗口
    private WindowManager wm;
    //窗口参数
    private WindowManager.LayoutParams lp;
    //窗口View
    private SessionLinearLayout mView;
    //监听Home
    private HomeWatcherReceiver mHomeKeyReceiver;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case StaticClass.SHOW_TIME_ERROR_WINDOW:
                    showTimeErrorWindow();
                    break;
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        L.i("TimeService onCreate");

        isTime();

        //注册Home监听广播
        mHomeKeyReceiver = new HomeWatcherReceiver();
        final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(mHomeKeyReceiver, homeFilter);
    }

    /**
     * 时间是否正确？
     */
    private void isTime() {
        L.i("isTime");
        new Thread(new Runnable() {
            @Override
            public void run() {
                //当前系统时间
                Date systemDate = new Date();
                String systemTime = format.format(systemDate);
                L.i("系统时间：" + systemTime);
                L.i("网络时间：" + getWebsiteDatetime());
                //如果时间不相等的话就显示window
                if (!systemTime.equals(getWebsiteDatetime())) {
                    handler.sendEmptyMessage(StaticClass.SHOW_TIME_ERROR_WINDOW);
                }
            }
        }).start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * 显示时间错误的提示
     */
    private void showTimeErrorWindow() {

        wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        lp = new WindowManager.LayoutParams();
        lp.width = android.view.WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = android.view.WindowManager.LayoutParams.MATCH_PARENT;
        lp.flags = android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lp.format = PixelFormat.TRANSLUCENT;
        //类型
        lp.type = android.view.WindowManager.LayoutParams.TYPE_PHONE;

        mView = (SessionLinearLayout) View.inflate(getApplicationContext(), R.layout.window_time_error_item, null);
        //返回
        mView.findViewById(R.id.btnCancal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wm.removeView(mView);
            }
        });
        //前往时间设置
        mView.findViewById(R.id.btnGoSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //go to time setting
                wm.removeView(mView);
                FunctionUtils.openSystemTime(getApplicationContext());
            }
        });
        //显示
        wm.addView(mView, lp);
        //返回键监听
        mView.setDispatchKeyEventListener(mDispatchKeyEventListener);
    }

    /**
     * 返回鍵监听
     */
    private SessionLinearLayout.DispatchKeyEventListener mDispatchKeyEventListener = new SessionLinearLayout.DispatchKeyEventListener() {

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            L.d("keyCode = " + event.getKeyCode());
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                if (mView.getParent() != null) {
                    wm.removeView(mView);
                }
                return true;
            }
            return false;
        }
    };


    /**
     * 监听Home键
     */
    class HomeWatcherReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(StaticClass.SYSTEM_DIALOG_REASON_KEY);
                if (StaticClass.SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {
                    if (mView.getParent() != null) {
                        wm.removeView(mView);
                    }
                }
            }
        }
    }

    /**
     * 获取指定网站的日期时间
     *
     * @return
     * @author SHANHY
     * @date 2015年11月27日
     */
    private String getWebsiteDatetime() {
        try {
            // 取得资源对象
            URL url = new URL("http://www.baidu.com");
            // 生成连接对象
            URLConnection uc = url.openConnection();
            // 发出连接
            uc.connect();
            // 读取网站日期时间
            long ld = uc.getDate();
            // 转换为标准时间对象
            Date date = new Date(ld);
            return format.format(date);
        } catch (MalformedURLException e) {
            Log.e("TAG", "Ma");
        } catch (IOException e) {
            Log.e("TAG", "IO");
        }
        return null;
    }

}
