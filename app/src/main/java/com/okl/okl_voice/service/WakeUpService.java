package com.okl.okl_voice.service;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.service
 *  文件名:   WakeUpService
 *  创建者:   LGL
 *  创建时间:  2016/8/8 17:35
 *  描述：    语音服务
 */

import android.app.Notification;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.search.sug.SuggestionSearch;
import com.baidu.mapapi.search.sug.SuggestionSearchOption;
import com.carlos.voiceline.mylibrary.VoiceLineView;
import com.google.gson.Gson;
import com.kymjs.rxvolley.RxVolley;
import com.kymjs.rxvolley.client.HttpCallback;
import com.okl.okl_voice.R;
import com.okl.okl_voice.activity.AllFunctionActivity;
import com.okl.okl_voice.activity.WebViewActivity;
import com.okl.okl_voice.adapter.ChatListAdapter;
import com.okl.okl_voice.adapter.NewsAdapter;
import com.okl.okl_voice.adapter.PoiAdapter;
import com.okl.okl_voice.entity.ChatListBean;
import com.okl.okl_voice.entity.NewsBean;
import com.okl.okl_voice.entity.PoiSuggBean;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.entity.UserEvent;
import com.okl.okl_voice.handler.NewsHandler;
import com.okl.okl_voice.handler.StockHandler;
import com.okl.okl_voice.handler.UnivoiceHandler;
import com.okl.okl_voice.handler.WeatherHandler;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.utils.OKhttpManager;
import com.okl.okl_voice.utils.SharePreUtils;
import com.okl.okl_voice.utils.TTSUtils;
import com.okl.okl_voice.view.SessionLinearLayout;
import com.squareup.okhttp.Request;
import com.univoice.asr.UnivoiceNluUtils;
import com.univoice.callback.OnChangeListener;
import com.univoice.speechrecord.AudioProcessHandler;
import com.univoice.speechrecord.OnAudioRecvListener;
import com.univoiceasr.OnASRResultListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cn.kuwo.autosdk.api.KWAPI;
import cn.kuwo.autosdk.api.PlayMode;
import cn.kuwo.autosdk.api.PlayState;

public class WakeUpService extends Service implements View.OnClickListener, Runnable {

    //支持本地
    private boolean isOfflineBNF = false;

    /////////////二次选择//////////////

    /**
     * 比如导航到天安门------为你找到天安门的多个地址，请说第几个选择，这个时候打开本地=true,同时，is导航 = true, iswakeup = true,
     * 然后我说：第二个，拿到结果，本地 = false, setsleep，判断是第几个：再判断是导航，再去导航
     */

    //导航
    private boolean isNavigationTwo = false;
    //新闻
    private boolean isNewsTwo = false;
    //联系人
    private boolean isContactTwo = false;
    //继续读
    private boolean isSpeakTwo = false;
    //是否拨打？
    private boolean isCallTwo = false;
    ////////////////////////////////
    //如果是唤醒的
    private boolean isWakeUps = false;
    //是否录音 可以不用管
    private boolean isAlive = true;
    //窗口管理器
    private WindowManager wm, wmProgress;
    //view
    private SessionLinearLayout mView;
    private View mProgressView;
    //天气
    private WindowManager wmWeather;
    private View mWeatherView;
    //标准文本
    private WindowManager wmText;
    private View mTextView;
    //来电
    private WindowManager wmAnswer;
    private View mAnswerView;
    //更多的地点
    private WindowManager wmPoiSugg;
    //poi的窗口View
    private View mPoiView;
    //新闻
    private WindowManager wmNews;
    private View mNewsView;
    //提示语
    private LinearLayout ll_text_content;
    //查看帮助
    private TextView look_help;
    //取消录音
    private ImageView iv_wm_back;
    //对话列表
    private ListView chatListView;
    //列表
    private ChatListAdapter mAdapter;
    // 会话数据源
    private List<ChatListBean> ListData = new ArrayList<>();
    //布局参数
    private WindowManager.LayoutParams layoutParams;
    //录音按钮
    private ImageView ivMicButton;
    //圆环进度
    private ProgressBar pro;
    //波纹线条
    private VoiceLineView voiceLine;
    //录音类
    private MediaRecorder mMediaRecorder;
    //录音线程
    private AudioProcessHandler mAudioProcessThread;
    //缓存大小
    int mReadBufSize = 160;
    // 1： short 0： byte
    int mReadStreamType = 1;
    //16K采样率
    int mSampleRate = 16000;
    //录音类
    public UnivoiceHandler mUnivoiceHandler;
    //TTS
    private TTSUtils mTTs;
    //定位
    public LocationClient mLocationClient = null;
    public BDLocationListener myListener = new MyLocationListener();
    // 百度地图反编码
    private GeoCoder geoCoder;
    // POI检索类
    private PoiSearch mPoiSearch;
    //新闻
    private NewsHandler newsHandler = new NewsHandler();
    //我的位置
    private String myAddress = null;
    // 包管理器
    private PackageManager mPackageManager;
    // 获取联系人集合
    private List<ResolveInfo> mAllApps;
    // 获取联系人
    private ResolveInfo res;
    // 包集合
    private List<String> mPackage = new ArrayList<String>();
    // 应用名集合
    private List<String> mName = new ArrayList<String>();
    // activity入口集合
    private List<String> mFirst = new ArrayList<String>();
    //蓝牙本地适配器
    private BluetoothAdapter mBluetoothAdapter;
    // 音量管理
    private AudioManager mAudioManager;
    //股票
    private StockHandler stockHandler = new StockHandler(new Gson());
    //天气
    private WeatherHandler weatherHandler = new WeatherHandler(new Gson());
    // 最大音量
    private int maxVolume;
    // 最小音量
    private int currentVolume;
    //wifi管理器
    private WifiManager wifiManager;
    //监听Home
    private HomeWatcherReceiver mHomeKeyReceiver;
    //导航地址
    private String navigation_address;
    //call phone的临时存储
    private String callPhone;
    //初始化控制类
    private UnivoiceNluUtils mUnivoiceNluUtils;
    //滴
    private SoundPool sp = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
    //更多的地点
    private SuggestionSearch mSuggestionSearch;
    //更多地点的数据
    private List<PoiSuggBean> mListSugg = new ArrayList<>();
    //修改唤醒词
    private NewWakeUpReceiver newWakeUpReceiver;
    //mediaplay
    private MediaPlayer mPlayer = null;
    //监听来电
    private BluetoothCallReceiver bluetoothCallReceiver;
    //酷我音乐接口
    private KWAPI mKwapi = null;
    //我的导航地址
    private String myPoiAddress = "";
    //测试log
    private TextView tvLog;
    private ScrollView testScrollView;

    //天气播报结束取消窗口
    private boolean isWeatherHide =  false;
    private boolean isTextViewHide = false;

    /**
     * 控制录音
     * true:在录音
     * false:不在录音
     */
    private boolean isRecording = true;

    //子线程
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case StaticClass.AUDIO_LINE_HANDLER:
                    if (mMediaRecorder == null) {
                        return;
                    }
                    double ratio = (double) mMediaRecorder.getMaxAmplitude() / 100;
                    double db = 0;// 分贝
                    //默认的最大音量是100,可以修改，但其实默认的，在测试过程中就有不错的表现
                    //你可以传自定义的数字进去，但需要在一定的范围内，比如0-200，就需要在xml文件中配置maxVolume
                    //同时，也可以配置灵敏度sensibility
                    if (ratio > 1) db = 20 * Math.log10(ratio);
                    //只要有一个线程，不断调用这个方法，就可以使波形变化
                    //主要，这个方法必须在ui线程中调用
                    voiceLine.setVolume((int) (db));
                    break;
                //录音结束
                case StaticClass.UNHANDLER_STOP_RECORD:
                    L.i("录音handler");
                    break;
                //说话喋喋不休
                case StaticClass.UNHANDLER_HAS_BEN_RECORD:
                    L.i("喋喋不休");

                    break;
                //清除View
                case StaticClass.CLEAR_LIST_VIEW:
                    if (mView.getParent() != null) {
                        wm.removeView(mView);
                    }
                    break;
                case StaticClass.CLEAR_LIST_VIEW_HANDLER:
                    if (mView.getParent() != null) {
                        wm.removeView(mView);
                        //这个会导致冲突
                        mTTs.startRead("不明白");
                    }
                    break;
                case StaticClass.TTS_START_SPEAK:
                    L.i("开始说话");
                    //是否在录音
                    if (isRecording) {
                        mAudioProcessThread.stop_rec();
                        isRecording = false;
                    }
                    break;
                case StaticClass.TTS_STOP_SPEAK:
                    L.i("结束说话");
                    //说完话之后咚的一下
                    //sp.play(1, 1, 1, 0, 0, 1);
                    L.e("7");
                    mPlayer.start();
                    L.e("8");
                    if (!isWakeUps) {
                        L.e("9");
                        if (isSpeakTwo | isCallTwo) {
                            L.e("10");
                            return;
                        }
                        L.e("11");
                        if (mView.getParent() != null) {
                            L.e("12");
                            handler.sendEmptyMessageDelayed(StaticClass.CLEAR_LIST_VIEW, 1000);
                            L.e("13");
                        }
                        L.e("14");
                    } else {
                        L.e("15");
                        isWakeUps = false;
                        L.e("16");
                    }
                    L.e("17");


                    //消失窗口
                    if(isWeatherHide){
                        //天气
                        if (mWeatherView != null) {
                            if (mWeatherView.getParent() != null) {
                                wmWeather.removeView(mWeatherView);
                                isWeatherHide = false;
                            }
                        }
                    }

                    if(isTextViewHide){
                        //textview
                        if (mTextView != null) {
                            if (mTextView.getParent() != null) {
                                wmText.removeView(mTextView);
                                isTextViewHide = false;
                            }
                        }
                    }

                    break;
            }
            super.handleMessage(msg);
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        L.i("WakeUpService onCreate");
        init();

    }

    /**
     * 初始化相关资源
     */
    private void init() {
        if (mKwapi == null) {
            mKwapi = KWAPI.createKWAPI(getApplicationContext(), "auto");
        }
        //绑定通知栏
        startNotification();
        //初始化Window
        initWindow();
        //初始化更多地点
        initSuggestionSearch();
        //初始化录音识别
        initAudioRecord();
        //初始化控制信息
        initNluUtils();
        //接收回调信息
        initUnivoiceHandler();
        //初始化TTS
        mTTs = TTSUtils.getInstance(getApplicationContext(), handler);
        //声明LocationClient类
        mLocationClient = new LocationClient(getApplicationContext());
        //注册监听函数
        mLocationClient.registerLocationListener(myListener);

        //初始化定位
        initLocation();

        //初始化播放器
        sp.load(getApplicationContext(), R.raw.tts_stop, 1);
        //初始化播放
        mPlayer = MediaPlayer.create(this, R.raw.tts_stop);
        //监听结束
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                L.e("铃声结束");
                if (!isRecording) {
                    mAudioProcessThread.start_rec();
                    isRecording = true;
                }
                L.e("19");
                //L.i("isSpeakTwo:" + isSpeakTwo + "---isOfflineBNF: " + isOfflineBNF);
                //已开启本地
                if (isOfflineBNF | isSpeakTwo | isCallTwo) {
                    L.e("20");
                    isSpeakTwo = false;
                    L.e("21");
                    //mAudioProcessThread.start_rec();
                    mUnivoiceHandler.isWakeUp = true;
                    L.i("继续----->");
                    L.e("22");
                }
            }
        });

        //POI
        initPoi();
        //获取所有App安装信息
        getPackage();
        //注册
        EventBus.getDefault().register(this);
        //获取本地蓝牙适配器
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //初始化wifi
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        //设置唤醒词灵敏度
        //new UniVoiceWakeUp().setWakeUpConfidenceThres(10.0f);

        //注册Home监听广播
        mHomeKeyReceiver = new HomeWatcherReceiver();
        final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        registerReceiver(mHomeKeyReceiver, homeFilter);

        //注册更改唤醒词的广播
        newWakeUpReceiver = new NewWakeUpReceiver();
        IntentFilter newWakeupFilter = new IntentFilter(StaticClass.NEW_WAKEUP_ACTION_BROADCAST);
        registerReceiver(newWakeUpReceiver, newWakeupFilter);

        //监听来电
        bluetoothCallReceiver = new BluetoothCallReceiver();
        IntentFilter blueCallFilter = new IntentFilter(StaticClass.BLUETOOTH_CALL_IN);
        registerReceiver(bluetoothCallReceiver, blueCallFilter);

        //刷新一下唤醒词
        //mUnivoiceHandler.reLoadWakeUpResource();


        if (StaticClass.isDeBug) {
            showTestWindow();
        }

    }

    /**
     * 初始化百度多样选择
     */
    private void initSuggestionSearch() {
        mSuggestionSearch = SuggestionSearch.newInstance();
        mSuggestionSearch.setOnGetSuggestionResultListener(Sugglistener);
    }

    /**
     * 多个地点的回调
     */
    OnGetSuggestionResultListener Sugglistener = new OnGetSuggestionResultListener() {
        public void onGetSuggestionResult(SuggestionResult res) {
            if (res == null || res.getAllSuggestions() == null) {
                return;
                //未找到相关结果
            }
            L.e("18");
            //mTTs.startRead("为你搜索到多个地点");
            mListSugg.clear();
            //获取在线建议检索结果
            for (SuggestionResult.SuggestionInfo info : res.getAllSuggestions()) {
                if (info.key != null) {
                    PoiSuggBean bean = new PoiSuggBean();
                    bean.setName(info.key);
                    bean.setLa(info.pt.latitude);
                    bean.setLo(info.pt.longitude);
                    mListSugg.add(bean);
                } else {
                    mTTs.startRead("未搜索到结果");
                    return;
                }
            }
            //导航的多个地点
            if (mListSugg.size() > 0) {
                mTTs.startRead("为你找到：" + myPoiAddress + "的多个地点,请说第几个选择，或说取消");
                L.i("myPoiAddress:" + myPoiAddress);
                //显示窗口
                showPoiSuggWindow(mListSugg);
                isOfflineBNF = true;
                isNavigationTwo = true;
                L.i("继续识别");
            } else {
                mTTs.startRead("未搜索到结果");
            }
        }
    };


    /**
     * 初始化控制信息
     */
    private void initNluUtils() {
        mUnivoiceNluUtils = new UnivoiceNluUtils(getApplicationContext());
    }

    /**
     * 初始化POI检索
     */
    private void initPoi() {
        // 初始化POI检索
        mPoiSearch = PoiSearch.newInstance();
        mPoiSearch.setOnGetPoiSearchResultListener(poiListener);
    }

    /**
     * 初始化地图相关/正反编码
     */
    private void initGeoCoder() {
        geoCoder = GeoCoder.newInstance();
        L.i("initGeoCoder");
        // 设置地理编码检索监听者
        geoCoder.setOnGetGeoCodeResultListener(new OnGetGeoCoderResultListener() {
            //反编码
            @Override
            public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    L.i("反错误：" + result.error);
                    return;
                }
                L.i("地址：" + result.getAddress());
            }

            //正编码
            @Override
            public void onGetGeoCodeResult(GeoCodeResult result) {
                //百度地图错误
                if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                    L.i("正错误：" + result.error);
                    listLeft("未知错误");
                    return;
                }
                //未查询到结果
                if (result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
                    listLeft("未找到地址");
                    return;
                }
                double la = result.getLocation().latitude;
                double lo = result.getLocation().longitude;
                L.i("位置：" + la + "," + lo);
                //开始导航
                try {
                    FunctionUtils.StartNavigation(getApplicationContext(), la, lo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listLeft("正在为你导航到：" + navigation_address);
                //关闭窗口
                wm.removeView(mView);
                L.i("结束");
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        L.i("onStartCommand");
        String button_start = intent.getStringExtra("button_start");
        //如果点击了按钮，就起来
        if ("button_start".equals(button_start)) {
            ivMicButton.setSelected(true);
            wm.addView(mView, layoutParams);
            //mUnivoiceHandler.isWakeUp = true;
            LineGone();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        L.i("onDestroy");
        if (mView.getParent() != null) {
            wm.removeView(mView);
        }
        //关闭录音资源
        isAlive = false;
        mMediaRecorder.release();
        mMediaRecorder = null;
        mPoiSearch.destroy();
        geoCoder.destroy();
        //取消注册
        EventBus.getDefault().unregister(this);
        //取消监听
        if (null != mHomeKeyReceiver) {
            unregisterReceiver(mHomeKeyReceiver);
        }
        if (null != newWakeUpReceiver) {
            unregisterReceiver(newWakeUpReceiver);
        }
        if (null != bluetoothCallReceiver) {
            unregisterReceiver(bluetoothCallReceiver);
        }

        mTTs.destoryTTs();
        //自启
        startService(new Intent(this, WakeUpService.class));
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.look_help:
                //点击帮助之后，关闭窗口，跳转All Setting
                if (mView.getParent() != null) {
                    wm.removeView(mView);
                }
                mUnivoiceHandler.setVoiceSleep();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), AllFunctionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //启动
                getApplicationContext().startActivity(intent);
                break;
            case R.id.iv_wm_back:
                L.i("按back按钮");
                //睡眠
                mUnivoiceHandler.setVoiceSleep();
                wm.removeView(mView);
                //防止崩溃
                try {
                    mTTs.startRead("已取消！");
                } catch (Exception e) {

                }
                break;
            //录音按钮
            case R.id.ivMicButton:
                ivMicButton.setSelected(!ivMicButton.isSelected());
                /**
                 * isButtonCheck为按钮的状态
                 * true:声波纹显示
                 * false:listview显示
                 */
                if (ivMicButton.isSelected()) {
                    L.i("on");
                    //显示
                    LineVisible();
                    mUnivoiceHandler.isWakeUp = true;
                } else {
                    L.i("off");
                    //隐藏
                    LineGone();
                    //直接去拿结果
                    //mUnivoiceHandler.setVoiceSleep();
                    UnivoiceHandler.onlineASR.stopAsr();
                }
                break;
        }
    }

    /**
     * 初始化录音线程
     */
    private void initAudioRecord() {
        //已经封装好的录音程序
        mAudioProcessThread = new AudioProcessHandler();
        mAudioProcessThread.SetSampleRate(mSampleRate);
        mAudioProcessThread.SetReadBufSize(mReadBufSize);
        mAudioProcessThread.SetReadStreamType(mReadStreamType);
        //录音返回数据的回调
        mAudioProcessThread.SetOnAudioRecvListener(mAudioRecvListener);
        //开启录音线程
        mAudioProcessThread.start();
        mAudioProcessThread.start_rec();
        isRecording = true;
        L.i("initAudioRecord");
    }

    /**
     * 初始化录音线程
     */
    private void initUnivoiceHandler() {
        mUnivoiceHandler = new UnivoiceHandler(getApplicationContext(), mAsrResultListener, handler);
        //内网地址
        //mUnivoiceHandler.setRequestUrl(StaticClass.Within_NET);
        //外网地址
        mUnivoiceHandler.setRequestUrl(StaticClass.OUTSIDE_NET);
        //初始化
        mUnivoiceHandler.init();
        L.i("initUnivoiceHandler");
    }

    /**
     * 隐藏文本提示，声波纹，显示listview
     */
    private void LineGone() {
        L.i("LineGone");
        pro.setVisibility(View.GONE);
        ll_text_content.setVisibility(View.GONE);
        voiceLine.setVisibility(View.GONE);
        chatListView.setVisibility(View.VISIBLE);
    }

    /**
     * 显示文本提示，声波纹，隐藏listview
     */
    private void LineVisible() {
        L.i("LineVisible");
        pro.setVisibility(View.VISIBLE);
        ll_text_content.setVisibility(View.VISIBLE);
        voiceLine.setVisibility(View.VISIBLE);
        chatListView.setVisibility(View.GONE);
    }

    /**
     * 显示列表左边
     *
     * @param text
     */
    public synchronized void listLeft(String text) {
        L.i("listLeft:" + text);
        mTTs.startRead(text);
        ChatListBean left = new ChatListBean();
        left.setType(ChatListAdapter.VALUE_LEFT_TEXT);
        left.setValue(text);
        ListData.add(left);
        try {
            //通知
            mAdapter.notifyDataSetChanged();
        } catch (Exception e) {

        }
        // 滚动到最底部
        chatListView.setSelection(chatListView.getBottom());
    }

    /**
     * 显示右边列表
     *
     * @param text
     */
    private synchronized void listRight(String text) {
        L.i("listRight");
        ChatListBean right = new ChatListBean();
        right.setType(ChatListAdapter.VALUE_RIGHT_TEXT);
        right.setValue(text);
        ListData.add(right);
        mAdapter.notifyDataSetChanged();
        // 滚动到最底部
        chatListView.setSelection(chatListView.getBottom());

    }

    /**
     * 初始化VoiceLineView
     *
     * @param mView
     */
    private void initVoiceLineView(View mView) {
        L.i("initVoiceLineView");
        //波纹
        voiceLine = (VoiceLineView) mView.findViewById(R.id.voiceLine);
        //初始化MediaRecorder
        if (mMediaRecorder == null) {
            mMediaRecorder = new MediaRecorder();
        }
        //设置参数
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        //输出log
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "audio.log");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mMediaRecorder.setOutputFile(file.getAbsolutePath());
        mMediaRecorder.setMaxDuration(1000 * 60 * 10);
        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaRecorder.start();
        //启动线程
        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * 子线程
     */
    @Override
    public void run() {
        while (isAlive) {
            handler.sendEmptyMessage(StaticClass.AUDIO_LINE_HANDLER);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 回调
     */
    OnAudioRecvListener mAudioRecvListener = new OnAudioRecvListener() {
        @Override
        public void OnAudioRecv(Object o) {
            short[] pcm_data = (short[]) o;
            mUnivoiceHandler.handleVoice(pcm_data, isOfflineBNF);
        }

        @Override
        public void OnAudioEOF() {

        }
    };

    /**
     * 解析结果
     */
    OnASRResultListener mAsrResultListener = new OnASRResultListener() {
        @Override
        public void ResultHandler(final String result) {
            L.i("WakeUpService 回调");
            mUnivoiceHandler.isOver = true;
            if (result.equals("wake up!")) {
                L.i("mAsrResultListener wake up");
                if (mView.getParent() == null) {
                    //加载view
                    wm.addView(mView, layoutParams);
                }

                listLeft("请说！");
                LineVisible();
                isWakeUps = true;
                L.i("我UP了");
                //当前如果有任何Window都要清理

                //天气
                if (mWeatherView != null) {
                    if (mWeatherView.getParent() != null) {
                        wmWeather.removeView(mWeatherView);
                    }
                }

                //新闻
                if (mNewsView != null) {
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                //poi
                if (mPoiView != null) {
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                }

                //textview
                if (mTextView != null) {
                    if (mTextView.getParent() != null) {
                        wmText.removeView(mTextView);
                    }
                }

                //进度
                if (mProgressView != null) {
                    if (mProgressView.getParent() != null) {
                        wmProgress.removeView(mProgressView);
                    }
                }

                return;
            }

            testScrollView.post(new Runnable() {
                @Override
                public void run() {
                    //打印log
                    tvLog.append(result + "\n");
                }
            });

            /**
             * 在这里判断如果是第二次就进入第二次识别
             */
            //L.i("isOfflineBNF:" + isOfflineBNF + "isNavigationTwo" + isNavigationTwo + "isNewsTwo: " + isNewsTwo + "isSpeakTwo:" + isSpeakTwo);
            if (isOfflineBNF) {
                handlerAsrResultTwo(result);
                return;
            }
            if (isRecording) {
                handlerAsrResult(result);
            }
        }
    };

    /**
     * 二次录音结果
     *
     * @param result
     */
    private void handlerAsrResultTwo(String result) {
        L.i("二次录音：" + result);

        isOfflineBNF = false;
        mUnivoiceHandler.setVoiceSleep();

        if (TextUtils.isEmpty(result)) {
            L.i("没有听清,请再说一遍");
            mTTs.startRead("没有听清,请再说一遍");
            isOfflineBNF = true;
            return;
        }

        //判断
        switch (result) {
            case "第1个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(0).getLa(), mListSugg.get(0).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(0).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(0);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                break;
            case "第2个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(1).getLa(), mListSugg.get(1).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(1).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(1);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                break;
            case "第3个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(2).getLa(), mListSugg.get(2).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(2).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(2);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }

                break;
            case "第4个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(3).getLa(), mListSugg.get(3).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(3).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(3);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                break;
            case "第5个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(4).getLa(), mListSugg.get(4).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(4).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(4);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }

                break;
            case "第6个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(5).getLa(), mListSugg.get(5).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(5).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(5);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                break;
            case "第7个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(6).getLa(), mListSugg.get(6).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(6).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(6);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }

                break;
            case "第8个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(7).getLa(), mListSugg.get(7).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(7).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(7);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }

                break;
            case "第9个":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    try {
                        FunctionUtils.StartNavigation(getApplicationContext(), mListSugg.get(8).getLa(), mListSugg.get(8).getLo());
                        //播放标题
                        mTTs.startRead("正在为你导航到：" + mListSugg.get(8).getName());
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    try {
                        startNewTwo(8);
                    } catch (Exception e) {
                        mTTs.startRead("超出条目");
                    }
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                }
                break;
            case "确定":
                //电话
                if (isCallTwo) {
                    mTTs.startRead("正在为你拨打" + callPhone);
                    FunctionUtils.CallPhone(getApplicationContext(), callPhone);
                    isCallTwo = false;
                    handler.sendEmptyMessage(StaticClass.CLEAR_LIST_VIEW);
                }
                break;
            case "取消":
                if (isNavigationTwo) {
                    isNavigationTwo = false;
                    mTTs.startRead("已取消");
                    if (mPoiView.getParent() != null) {
                        wmPoiSugg.removeView(mPoiView);
                    }
                } else if (isNewsTwo) {
                    isNewsTwo = false;
                    mTTs.startRead("已取消");
                    if (mNewsView.getParent() != null) {
                        wmNews.removeView(mNewsView);
                    }
                } else if (isCallTwo) {
                    isNavigationTwo = false;
                    mTTs.startRead("已取消");
                    if (mView.getParent() != null) {
                        wm.removeView(mView);
                    }
                }
                break;
            case "接听":
                //这里只有接听

                break;
            case "挂断":
                //这里只有挂断

                break;
            default:
                L.e("不明白，请再说一遍");
                mTTs.startRead("不明白，请再说一遍或取消");
                isOfflineBNF = true;
                break;
        }
    }

    /**
     * 新闻
     *
     * @param index
     * @throws Exception 异常说明超出条目
     */
    private void startNewTwo(int index) throws Exception {
        Intent intent = new Intent(WakeUpService.this, WebViewActivity.class);
        intent.putExtra("news_title", listResponse.get(index).getTitle());
        intent.putExtra("news_url", listResponse.get(index).getUrl());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        //播放标题
        mTTs.startRead("正在为你搜索：" + listResponse.get(index).getTitle());
    }

    /**
     * 开启通知绑定
     */
    private void startNotification() {
        L.i("startNotification");
        // 灰色保活
        if (Build.VERSION.SDK_INT < 18) {
            // API < 18
            startForeground(StaticClass.GRAY_SERVICE_ID, new Notification());
            // 此方法能有效隐藏Notification上的图标
        } else {
            Intent innerIntent = new Intent(this, GrayInnerService.class);
            startService(innerIntent);
            startForeground(StaticClass.GRAY_SERVICE_ID, new Notification());
        }
    }

    /**
     * 给 API >= 18 的平台上用的灰色保活手段
     */

    public static class GrayInnerService extends Service {

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            // 设置前台通知
            startForeground(StaticClass.GRAY_SERVICE_ID, new Notification());
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public IBinder onBind(Intent intent) {

            return null;
        }
    }

    /**
     * 显示窗口
     */
    private void initWindow() {
        L.i("initWindow");
        //窗口管理器
        wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.flags =
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
        //WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | 沒有焦点
        //格式
        layoutParams.format = PixelFormat.TRANSLUCENT;
        //类型
        layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;

        mView = (SessionLinearLayout) View.inflate(getApplicationContext(), R.layout.layout_window_item, null);

        ll_text_content = (LinearLayout) mView.findViewById(R.id.ll_text_content);
        ivMicButton = (ImageView) mView.findViewById(R.id.ivMicButton);
        ivMicButton.setOnClickListener(this);

        initVoiceLineView(mView);

        pro = (ProgressBar) mView.findViewById(R.id.pro);
        look_help = (TextView) mView.findViewById(R.id.look_help);
        look_help.setText(Html.fromHtml("<u>" + getString(R.string.look_help) + "</u>"));
        look_help.setOnClickListener(this);

        iv_wm_back = (ImageView) mView.findViewById(R.id.iv_wm_back);
        iv_wm_back.setOnClickListener(this);

        chatListView = (ListView) mView.findViewById(R.id.chatListView);
        mAdapter = new ChatListAdapter(getApplicationContext(), ListData);
        chatListView.setAdapter(mAdapter);

        //默认显示文字
        ChatListBean left = new ChatListBean();
        left.setType(ChatListAdapter.VALUE_LEFT_TEXT);
        left.setValue("欢迎使用欧克蓝语音，你可以说关键字来唤醒语音管家！");
        ListData.add(left);
        mAdapter.notifyDataSetChanged();

        //监听按键
        mView.setDispatchKeyEventListener(mDispatchKeyEventListener);
    }

    /**
     * 返回鍵监听
     */
    private SessionLinearLayout.DispatchKeyEventListener mDispatchKeyEventListener = new SessionLinearLayout.DispatchKeyEventListener() {

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            L.d("keyCode = " + event.getKeyCode());
            if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                if (mView.getParent() != null) {
                    wm.removeView(mView);
                }
                mUnivoiceHandler.setVoiceSleep();
                mTTs.startRead("已取消");
                return true;
            }
            return false;
        }
    };


    /**
     * 显示listview
     */
    private void closeAsr() {
        mUnivoiceHandler.setVoiceSleep();
        LineGone();
    }

    /**
     * 处理录音的结果
     *
     * @param resultText
     */
    private void handlerAsrResult(String resultText) {
        L.i("录音的结果:" + resultText);

        mUnivoiceHandler.setVoiceSleep();
        /**
         * 录音有结果了，如果不为空的话，我就去操作
         * 如果为空，就return
         */
        if (TextUtils.isEmpty(resultText)) {
            L.i("Sleep");
            //没有听清
            closeAsr();
            //0 ++ = 1 1++ = 2
            if (isSpeakTwo) {
                isSpeakTwo = false;
                listLeft("你不说话，那我退下了！");
                LineGone();
            } else {
                //继续
                isSpeakTwo = true;
                listLeft("没有听清！请再说一遍");
                LineVisible();
            }
            return;
        }

        //添加到listview
        listRight(resultText);

        //自定义词汇
        if (resultText.equals("你好")) {
            listLeft("你也好！");
            closeAsr();
            return;
        } else if (resultText.equals("现在是什么时间")) {
            //设置日期格式
            SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
            String time = df.format(new Date());
            listLeft("现在是：" + time);
            L.i("现在的时间：" + time);
            closeAsr();
            return;
        } else if (resultText.equals("电话") || resultText.equals("打电话")) {
            listLeft("打电话给谁？");
            closeAsr();
            return;
        } else if (resultText.equals("音乐") || resultText.equals("播放音乐")
                || resultText.equals("歌曲") || resultText.equals("播放歌曲")
                || resultText.equals("放音乐") || resultText.equals("放歌曲")
                || resultText.equals("想听歌曲")) {
            listLeft("想听什么歌？");
            closeAsr();
            return;
        } else if (resultText.equals("导航")) {
            listLeft("导航到哪儿？");
            closeAsr();
            return;
        } else if (resultText.equals("打开")) {
            listLeft("打开什么？");
            closeAsr();
            return;
        } else if (resultText.equals("关闭")) {
            listLeft("关闭什么？");
            closeAsr();
            return;
        } else if (resultText.contains("笑话")) {
            showProgress();
            //笑话
            RxVolley.get(StaticClass.JOKE_URL, new HttpCallback() {
                @Override
                public void onSuccess(String t) {
                    wmProgress.removeView(mProgressView);
                    L.i("笑话json:" + t);
                    try {
                        JSONObject jsonObject = new JSONObject(t);
                        JSONObject jsonResult = jsonObject.getJSONObject("result");
                        JSONArray jsonArray = jsonResult.getJSONArray("data");
                        JSONObject jsonJock = (JSONObject) jsonArray.get(0);
                        listLeft(jsonJock.getString("content"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            closeAsr();
            return;
        } else if (resultText.equals("返回主页")) {
            listLeft("正在为你返回主页");
            FunctionUtils.goHome(getApplicationContext());
            closeAsr();
            return;
        }
        //睡眠
        //mUnivoiceHandler.setVoiceSleep();
        L.i("Sleep");
        //显示对话列表
        LineGone();

        //解析得到的json
        //转换json
        final String json = mUnivoiceNluUtils.processResult(resultText);
        L.i("json:" + json);
        //如果json不为空的话，实际上也不会为空
        if (!TextUtils.isEmpty(json)) {
            String resultOriginal = null;
            JSONArray jsonArray = null;
            String appName = null;
            String className = null;
            String videoName = null;
            //开始解析json
            try {
                JSONObject jsonObject = new JSONObject(json);
                String resultType = jsonObject.getString("resultType");
                if (!jsonObject.isNull("originalText")) {
                    resultOriginal = jsonObject.getString("originalText");
                } else if (!jsonObject.isNull("appName")) {
                    appName = jsonObject.getString("appName");
                } else if (!jsonObject.isNull("videoName")) {
                    videoName = jsonObject.getString("videoName");
                } else if (!jsonObject.isNull("className")) {
                    className = jsonObject.getString("className");
                } else {
                    jsonArray = jsonObject.getJSONArray("resultData");
                }
                //开始识别指令
                switch (resultType) {
                    //没有找到该号码
                    case "NO_NUMBER":
                        //返回结果
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //没有此联系人 纯数字
                    case "NO_PERSON":
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonNoPersion = (JSONObject) jsonArray.get(i);
                            //得到的号码
                            callPhone = jsonNoPersion.getString("phoneNumber");
                            if (callPhone.equals("10086")) {
                                listLeft("是否拨打中国移动客服，请说确定或取消！");
                            } else if (callPhone.equals("10000")) {
                                listLeft("是否拨打中国电信客服，请说确定或取消！");
                            } else if (callPhone.equals("10010")) {
                                listLeft("是否拨打中国联通客服，请说确定或取消！");
                            } else {
                                listLeft(jsonObject.getString("resultText") + "请说确定或取消！");
                            }
                            //这个是ok or no
                            //isOKorNo = true;
                            isCallTwo = true;
                            isOfflineBNF = true;
                            LineVisible();
                        }
                        break;
                    //多个号码
                    case "MULTIPLE_NUMBERS":
                        listLeft("查询到多个号码" + jsonObject.getString("resultText"));
                        //显示列表
                        //isSecond = true;
                        break;
                    //拨打号码
                    case "CONFIRM_CALL":
                        //如果确定/取消
                        //暂时先直接拨打电话
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonCall = (JSONObject) jsonArray.get(i);
                            //call
                            FunctionUtils.CallPhone(getApplicationContext(), jsonCall.getString("phoneNumber"));
                            listLeft("正在为你呼叫" + jsonCall.getString("contactName"));
                        }
                        break;
                    //短信
                    case "SMS_Send":
                        listLeft("暂不支持的功能");
                        break;
                    //播放歌曲
                    case "Media_PlaySong":
                        //解析
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonMusicSong = (JSONObject) jsonArray.get(i);
                            if (jsonObject.getString("resultText").equals("没有找到该歌曲")) {
                                listLeft(jsonObject.getString("resultText"));
                                break;
                            }
                            //判断有歌名
                            if (!TextUtils.isEmpty(jsonMusicSong.getString("songName"))) {
                                listLeft("正在为你准备播放：" + jsonMusicSong.getString("songName"));
                                //FunctionUtils.PlayMusic(getApplicationContext(), jsonMusicSong.getString("songName"), "", "");
                                mKwapi.playClientMusics(getApplicationContext(), jsonMusicSong.getString("songName"), "", "");
                                //跳出break
                                wm.removeView(mView);
                                //有歌手
                            } else if (!TextUtils.isEmpty(jsonMusicSong.getString("artistName"))) {
                                listLeft(jsonObject.getString("resultText"));
                                //FunctionUtils.PlayMusic(getApplicationContext(), "", jsonMusicSong.getString("artistName"), "");
                                mKwapi.playClientMusics(getApplicationContext(), "", jsonMusicSong.getString("artistName"), "");
                                //跳出break
                                wm.removeView(mView);
                            } else if (TextUtils.isEmpty("songName") && TextUtils.isEmpty("artistName")) {
                                listLeft(jsonObject.getString("resultText"));
                            }
                            mKwapi.setPlayState(getApplicationContext(), PlayState.STATE_PLAY);
                        }
                        break;
                    //播放列表我爱听的
                    case "Media_PlayPlaylist":
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //播放文件夹我不爱听的
                    case "Media_PlayFolder":
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //我最喜欢的歌曲
                    case "Media_PlayFavorites":
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //列表播放
                    case "Media_SetRepeatMode":
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //上一首
                    case "Media_PlayNext":
                        listLeft(jsonObject.getString("resultText"));
                        setPlayModel(6);
                        break;
                    //下一首
                    case "Media_PlayPrevious":
                        listLeft(jsonObject.getString("resultText"));
                        setPlayModel(7);
                        break;
                    //开启导航
                    case "Navi_OpenNavi":
                        listLeft(jsonObject.getString("resultText"));
                        ComponentName com = new ComponentName(
                                "com.autonavi.minimap",
                                "com.autonavi.map.activity.SplashActivity");
                        Intent in = new Intent();
                        in.setComponent(com);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(in);
                        break;
                    //查找位置，xx省xx市xx区......
                    case "Navi_FindAddress":
                        //百度or高德
                        if (StaticClass.isBaidu) {
                            //正反编码
                            initGeoCoder();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonFindAddress = (JSONObject) jsonArray.get(i);
                                String city = jsonFindAddress.getString("cityName");
                                String address = jsonFindAddress.getString("districtName") + jsonFindAddress.getString("poiName")
                                        + jsonFindAddress.getString("streetName") + jsonFindAddress.getString("houseNumber");
                                if (TextUtils.isEmpty(city)) {
                                    geoCoder.geocode(new GeoCodeOption().address(address)
                                            .city(""));
                                } else {
                                    geoCoder.geocode(new GeoCodeOption().address(address)
                                            .city(city));
                                }
                                navigation_address = city + address;
                                L.i(navigation_address);
                            }
                        } else if (StaticClass.isAmap) {
                            String url = "";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonFindAddress = (JSONObject) jsonArray.get(i);
                                String city = jsonFindAddress.getString("cityName");
                                String address = jsonFindAddress.getString("districtName") + jsonFindAddress.getString("poiName")
                                        + jsonFindAddress.getString("streetName") + jsonFindAddress.getString("houseNumber");
                                L.i("city:" + city + ",address" + address);

                                if (TextUtils.isEmpty(city) && address.equals("世界之窗")) {
                                    url = "http://restapi.amap.com/v3/geocode/geo?key=" + StaticClass.AMAP_KEY + "&s=rsv3&address=" + "深圳市" + address;
                                } else if (TextUtils.isEmpty(city) && address.equals("东方明珠")) {
                                    url = "http://restapi.amap.com/v3/geocode/geo?key=" + StaticClass.AMAP_KEY + "&s=rsv3&address=" + "上海市" + address;
                                } else {
                                    url = "http://restapi.amap.com/v3/geocode/geo?key=" + StaticClass.AMAP_KEY + "&s=rsv3&address=" + city + address;
                                }
                                L.i(url);
                            }
                            //解析Json
                            RxVolley.get(url, new HttpCallback() {
                                @Override
                                public void onSuccess(String t) {
                                    L.i("成功：" + t);
                                    ParsingNavigationJson(t);
                                }

                                @Override
                                public void onFailure(int errorNo, String strMsg) {
                                    L.i("失败");
                                }
                            });
                        } else if (StaticClass.isSugg) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonFindAddress = (JSONObject) jsonArray.get(i);
                                String city = jsonFindAddress.getString("cityName");
                                String address = jsonFindAddress.getString("districtName") + jsonFindAddress.getString("poiName")
                                        + jsonFindAddress.getString("streetName") + jsonFindAddress.getString("houseNumber");
                                L.i("要去的地点：" + city + ": " + address);
                                //检索
                                if (!TextUtils.isEmpty(city) && !TextUtils.isEmpty(address)) {
                                    mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                                            .keyword(address)
                                            .city(city));
                                } else if (TextUtils.isEmpty(city)) {
                                    mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                                            .keyword(address)
                                            .city(address));
                                } else if (TextUtils.isEmpty(address)) {
                                    mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                                            .keyword(city)
                                            .city(city));
                                }
                            }
                        }
                        break;
                    //我的位置
                    case "Navi_WhereAmI":
                        // listLeft(jsonObject.getString("resultText"));
                        //定位
                        mLocationClient.start();
                        showProgress();
                        break;
                    //某个地方的交通情况
                    case "Traffic_Query":
                        listLeft(jsonObject.getString("resultText"));
                        break;
                    //某个地方/区的地铁或者公交 //附近的小吃
                    case "Navi_SearchPoiType":
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonPoi = (JSONObject) jsonArray.get(i);
                            if (jsonPoi.isNull("poiCategory")) {
                                postErrorEvent("不太明白主人的意思");
                                break;
                            }
                            if (jsonPoi.isNull("poiName")) {
                                postErrorEvent("请说出具体地址");
                                break;
                            }
                            String city = jsonPoi.getString("poiName");
                            String keyword = jsonPoi.getString("poiCategory");
                            mPoiSearch.searchInCity((new PoiCitySearchOption()).city(city).keyword(keyword).pageNum(1));
                        }
                        break;
                    //某个地点附近的地铁或者公交
                    case "Navi_SearchPoiTypeByPos":
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonPoi = (JSONObject) jsonArray.get(i);
                            String keyword = jsonPoi.getString("poiCategory");
                            String city = jsonPoi.getString("poiName");
                            //开始poi
                            if (!TextUtils.isEmpty(city) && !TextUtils.isEmpty(keyword)) {
                                //开始搜索
                                mPoiSearch.searchInCity((new PoiCitySearchOption()).city(city).keyword(keyword).pageNum(1));
                                listLeft(jsonObject.getString("resultText"));
                            } else if (TextUtils.isEmpty(city)) {
                                listLeft("请输出详细地址！");
                            } else if (TextUtils.isEmpty(keyword)) {
                                listLeft("请说的再详细一些");
                            }
                        }
                        break;
                    //新闻
                    case "NEWS_QUERY":
                        L.i("继续识别");
                        showNewsDialog();
                        if (mView.getParent() != null) {
                            wm.removeView(mView);
                        }
                        closeAsr();
                        //睡眠
                        L.i("Sleep");
                        break;
                    //天气
                    case "Weather_Query":
                        showProgress();
                        //listLeft(jsonObject.getString("resultText"));
                        weatherHandler.doFirstChoose(json, new OKhttpManager.ResultCallback<String>() {
                            //成功
                            @Override
                            public void onResponse(String response) {
                                try {
                                    //结果
                                    String jsonHandler = weatherHandler.handleWeatherResponse(response);
                                    L.i("天气:" + jsonHandler);
                                    //解析Json
                                    JSONObject jsonWeather = new JSONObject(jsonHandler);
                                    JSONArray jsonArrayData = jsonWeather.getJSONArray("resultData");
                                    //在这里判断JSONArray的长度排判断几天
                                    String weather = "";
                                    String time = "";
                                    String city = "";
                                    String wind = "";
                                    String direction = "";
                                    String t1 = "";
                                    String t2 = "";
                                    for (int i = 0; i < jsonArrayData.length(); i++) {
                                        JSONObject jsonData = (JSONObject) jsonArrayData.get(i);
                                        city = jsonWeather.getString("cityName");
                                        weather = jsonData.getString("s1");
                                        time = jsonWeather.getString("dateTime");
                                        wind = jsonData.getString("p1");
                                        direction = jsonData.getString("d1");
                                        t1 = jsonData.getString("t1");
                                        t2 = jsonData.getString("t2");
                                    }
                                    //关闭进度
                                    wmProgress.removeView(mProgressView);
                                    //mTTs.startRead(weather);
                                    //显示天气
                                    showWeatherWindow(city, weather, wind, direction, t1, t2, time);
                                } catch (JSONException e) {
                                    mTTs.startRead("天气查询失败");
                                    //显示天气
                                    showTextView("天气查询失败");
                                }
                            }

                            @Override
                            public void onError(Request request, Exception e) {
                                postErrorEvent("天气查询失败");
                            }
                        });
                        break;
                    //股票查詢
                    case "Stock_Query":
                        showProgress();
                        L.i("股票");
                        stockHandler.doFirstChoose(json, new OKhttpManager.ResultCallback<String>() {
                            @Override
                            public void onResponse(String response) {
                                if (mProgressView.getParent() != null) {
                                    wmProgress.removeView(mProgressView);
                                }
                                //结果
                                String[] jsonHandler = stockHandler.handleStockResponse(response);
                                if (jsonHandler.length > 4) {
                                    String stocks = jsonHandler[0] + "今天开盘价" + jsonHandler[1] + "昨日收盘价"
                                            + jsonHandler[2] + "当前价格" + jsonHandler[3];
                                    L.i(stocks);
                                    mTTs.startRead(stocks);
                                    isTextViewHide = true;
                                    showTextView(stocks);
                                } else {
                                    mTTs.startRead("查找到的信息有误！");
                                    showTextView("查找到的信息有误！");
                                }
                            }

                            @Override
                            public void onError(Request request, Exception e) {
                                L.i("失败");
                                postErrorEvent("股票查询失败");
                            }
                        });
                        break;
                    //电影
                    case "Media_PlayVideo":
                        if (!TextUtils.isEmpty(videoName)) {
                            listLeft("正在为你播放电影：" + videoName);
                        } else {
                            listLeft("播放什么电影？");
                        }
                        break;
                    //打开 app
                    case "Open_App":
                        if (!TextUtils.isEmpty(appName)) {
                            if (appName.equals("地图")) {
                                ComponentName componet = new ComponentName(
                                        "com.autonavi.minimap",
                                        "com.autonavi.map.activity.SplashActivity");
                                Intent i = new Intent();
                                i.setComponent(componet);
                                startActivity(i);
                                listLeft("正在打开" + appName);
                                break;
                            }
                            try {
                                openAPP(appName);
                                listLeft("正在打开" + appName);
                                wm.removeView(mView);
                            } catch (Exception e) {
                                listLeft("程序未安装");
                            }
                        } else {
                            listLeft("打开哪个应用？");
                        }
                        break;
                    //关闭app
                    case "Close_App":
                        if (!TextUtils.isEmpty(appName)) {
                            if (appName.equals("导航")) {
                                listLeft("正在关闭导航");
                                FunctionUtils.EndApp(getApplicationContext(), StaticClass.MAP_PACKAGE);
                                break;
                            }
                            if (appName.equals("歌曲")) {
                                listLeft("正在为你关闭歌曲");
                                FunctionUtils.EndApp(getApplicationContext(), StaticClass.KUWO_MUSIC_PACKAGE);
                                break;
                            }
                            try {
                                FunctionUtils.EndApp(getApplicationContext(), mPackage.get(mName.indexOf(appName)));
                                listLeft("正在关闭" + appName);
                            } catch (Exception e) {
                                listLeft("程序未安装");
                            }
                        } else {
                            listLeft("关闭哪个应用？");
                        }
                        break;
                    //控制设置
                    case "Setting":
                        switch (className) {
                            //增大音量
                            case "Volume_Raise":
                                listLeft("正在为你增大音量");
                                setVolume(1);
                                break;
                            //减小音量
                            case "Volume_Lower":
                                listLeft("正在为你减小音量");
                                setVolume(0);
                                break;
                            //最大音量
                            case "Volume_Max":
                                listLeft("正在为你调节至最大音量");
                                setVolume(3);
                                break;
                            //最小音量
                            case "Volume_Min":
                                listLeft("正在为你调节至最小音量");
                                setVolume(4);
                                break;
                            //打开wifi
                            case "WIFI_Open":
                                if (wifiManager.isWifiEnabled()) {
                                    listLeft("无线网络是开启状态");
                                    break;
                                }
                                wifiManager.setWifiEnabled(true);
                                listLeft("正在为你打开无线网络");
                                break;
                            //关闭wifi
                            case "WIFI_Close":
                                if (!wifiManager.isWifiEnabled()) {
                                    listLeft("无线网络是关闭状态状态");
                                    break;
                                }
                                wifiManager.setWifiEnabled(false);
                                listLeft("正在为你关闭无线网络");
                                break;
                            //打开蓝牙
                            case "Bluetooth_Open":
                                //判断是否硬件支持蓝牙
                                if (mBluetoothAdapter == null) {
                                    listLeft("当前设备不支持蓝牙'");
                                    break;
                                }
                                //判断是否打开蓝牙
                                if (mBluetoothAdapter.isEnabled()) {
                                    listLeft("蓝牙是开启状态");
                                    break;
                                }
                                //不做提示，强行打开
                                mBluetoothAdapter.enable();
                                listLeft("正在为你打开蓝牙");
                                break;
                            //关闭蓝牙
                            case "Bluetooth_Close":
                                //判断是否硬件支持蓝牙
                                if (mBluetoothAdapter == null) {
                                    listLeft("当前设备不支持蓝牙'");
                                    break;
                                }
                                //判断是否打开蓝牙
                                if (!mBluetoothAdapter.isEnabled()) {
                                    listLeft("蓝牙是关闭状态");
                                    break;
                                }
                                mBluetoothAdapter.disable();
                                listLeft("正在为你关闭蓝牙");
                                break;
                            //亮度+
                            case "Light_Lower":
                                listLeft("正在为你调高亮度");
                                FunctionUtils.setScreenPlay(getApplicationContext(), true, 10);
                                break;
                            //亮度-
                            case "Light_Raise":
                                listLeft("正在为你调低亮度");
                                FunctionUtils.setScreenPlay(getApplicationContext(), false, 10);
                                break;
                        }
                        break;
                    //收音机
                    case "Radio_OpenFrequence":
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonFm = (JSONObject) jsonArray.get(i);
                            JSONArray jsonWave = jsonFm.getJSONArray("waveBandList");
                            String fmOpen = jsonWave.getString(0);
                            String freq = jsonFm.getString("frequenceNumber");
                            listLeft("正在为你" + fmOpen + freq);
                            FunctionUtils.setFM(getApplicationContext(), "", freq);
                            L.i("调频");
                        }
                        break;
                    default:
                        listLeft("暂不支持的消息类型");
                        break;
                }
            } catch (JSONException e) {
                L.i("json解析失败");
                listLeft("发生未知错误");
            }
        } else {
            listLeft("不明白！");
            LineGone();
        }
    }

    /**
     * 解析导航Json
     *
     * @param t
     */
    private void ParsingNavigationJson(String t) {
        try {
            JSONObject object = new JSONObject(t);
            JSONArray geocodes = object.getJSONArray("geocodes");
            if (geocodes.length() == 0) {
                postErrorEvent("未搜索到结果！");
            } else if (geocodes.length() == 1) {
                JSONObject trueAddress = geocodes.getJSONObject(0);
                String location = trueAddress.getString("location");
                //listLeft("正在为你导航到：" + trueAddress.getString("formatted_address"));
                myPoiAddress = trueAddress.getString("formatted_address");
                String lngX = location.split(",")[0];
                String latY = location.split(",")[1];
                L.i(latY + "," + lngX);

                L.e("1");
                mSuggestionSearch.requestSuggestion((new SuggestionSearchOption())
                        .keyword(trueAddress.getString("formatted_address"))
                        .city(trueAddress.getString("province")));
                L.e("2");
                L.i("开始poi");
                //FunctionUtils.StartNavigation(getApplicationContext(), Double.parseDouble(latY), Double.parseDouble(lngX));
                //取消window
                L.e("3");
                if (mView.getParent() != null) {
                    L.e("4");
                    wm.removeView(mView);
                    L.e("5");
                }
                L.e("6");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<NewsBean> listResponse = new ArrayList<>();

    /**
     * 新闻
     */
    private void showNewsDialog() {
        //显示进度
        showProgress();
        newsHandler.doFirstChoose(new OKhttpManager.ResultCallback<List<NewsBean>>() {
            @Override
            public void onResponse(List<NewsBean> response) {
                //隐藏进度
                wmProgress.removeView(mProgressView);
                mTTs.startRead("为你找到多条新闻，请说第几个选择或取消");
                isOfflineBNF = true;
                isNewsTwo = true;
                listResponse = response;
                L.i("新闻");
                NewWindow(response);
            }
            @Override
            public void onError(Request request, Exception e) {
                postErrorEvent("没有找到新闻");
            }
        });
    }

    /**
     * 初始化定位
     */
    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("bd09ll");
        int span = 0;
        //可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setScanSpan(span);
        //可选，设置是否需要地址信息，默认不需要
        option.setIsNeedAddress(true);
        //可选，默认false,设置是否使用gps
        option.setOpenGps(true);
        //可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setLocationNotify(true);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationDescribe(true);
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIsNeedLocationPoiList(true);
        //可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        option.setIgnoreKillProcess(false);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        option.SetIgnoreCacheException(false);
        //可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        option.setEnableSimulateGps(false);
        mLocationClient.setLocOption(option);
    }

    /**
     * 定位回調
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // GPS定位结果
            if (location.getLocType() == BDLocation.TypeGpsLocation) {
                L.i("你当前的位置:" + location.getAddrStr());
                myAddress = location.getAddrStr();
                // 网络定位结果
            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
                L.i("你当前的位置:" + location.getAddrStr());
                myAddress = location.getAddrStr();
            } else {
                listLeft("无法获取呢当前的位置");
                if (mProgressView.getParent() != null) {
                    wmProgress.removeView(mProgressView);
                }
            }
            if (mProgressView.getParent() != null) {
                wmProgress.removeView(mProgressView);
            }
            // 位置语义化信息
            mTTs.startRead("你当前的位置:" + myAddress + location.getLocationDescribe());
            showTextView("你当前的位置:" + myAddress + location.getLocationDescribe());
        }
    }

    /**
     * POI检索回调
     */
    OnGetPoiSearchResultListener poiListener = new OnGetPoiSearchResultListener() {
        public void onGetPoiResult(PoiResult result) {
            L.i("POI 成功" + result.error);
            // 获取POI检索结果
            if (result.error == SearchResult.ERRORNO.NO_ERROR) {
                for (PoiInfo poiInfo : result.getAllPoi()) {
                    L.i(poiInfo.name + ":" + poiInfo.address + ":" + poiInfo.location.latitude + "," + poiInfo.location.longitude);
                }
            }
        }

        public void onGetPoiDetailResult(PoiDetailResult result) {
            L.i("POI 失败");
            // 检索结果正常返回
            if (result.error == SearchResult.ERRORNO.NO_ERROR) {

            }
        }
    };

    /**
     * 获取所有安装应用信息的方法
     */
    private void getPackage() {
        L.i("getPackage");
        mPackageManager = getPackageManager();
        // 获取手机上所有的包
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        // 设置Category
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mAllApps = mPackageManager.queryIntentActivities(mainIntent, 0);
        // 按报名排序
        Collections.sort(mAllApps, new ResolveInfo.DisplayNameComparator(
                mPackageManager));
        for (int i = 0; i < mAllApps.size(); i++) {
            res = mAllApps.get(i);
            // 把数据装载在集合里面
            mPackage.add(res.activityInfo.packageName);
            mName.add(res.loadLabel(mPackageManager).toString());
            mFirst.add(res.activityInfo.name);
        }
    }


    /**
     * 打开应用
     *
     * @param APPName 应用名
     */
    private void openAPP(String APPName) {
        L.i("应用名：" + APPName);
        L.i("包名：" + mPackage.get(mName.indexOf(APPName)));
        L.i("入口：" + mFirst.get(mName.indexOf(APPName)));
        ComponentName componet = new ComponentName(mPackage.get(mName
                .indexOf(APPName)), mFirst.get(mName.indexOf(APPName)));
        Intent i = new Intent();
        i.setComponent(componet);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }

    //主线程接收消息
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserEvent(UserEvent event) {
        //如果多个消息，可在实体类中添加type区分消息
        int type = event.getType();
        L.i("eventbus type :" + type);
        if (type == 0) {
            listLeft(event.getError());
        } else if (type == 1) {
            listLeft(event.getWeather());
        } else if (type == 2) {
            listLeft(event.getStock());
        } else if (type == 3) {
            listLeft(event.getNavigation());
        } else if (type == 4) {
            listLeft(event.getNews());
        }
    }

    /**
     * 错误的查询，子线程更新UI
     *
     * @param text
     */
    private void postErrorEvent(String text) {
        UserEvent event = new UserEvent();
        event.setType(0);
        event.setError(text);
        EventBus.getDefault().post(event);

    }

    /**
     * 音量+/-
     */
    public void setVolume(int flag) {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        /*
         * 获取到最大/最小音量
		 */
        maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        L.i("最大音量：" + maxVolume);
        currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        L.i("最小音量：" + currentVolume);
        // 降低音量，调出系统音量控制
        if (flag == 0) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER,
                    AudioManager.FX_FOCUS_NAVIGATION_UP);
            // 增加音量，调出系统音量控制
        } else if (flag == 1) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE,
                    AudioManager.FX_FOCUS_NAVIGATION_UP);
            //最大音量
        } else if (flag == 2) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
            //最小音量
        } else if (flag == 3) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0);
        }
    }


    /**
     * 监听Home键
     */
    class HomeWatcherReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(StaticClass.SYSTEM_DIALOG_REASON_KEY);
                if (StaticClass.SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {
                    if (mView.getParent() != null) {
                        wm.removeView(mView);
                    }
                    mUnivoiceHandler.setVoiceSleep();
                }
            }
        }
    }

    /**
     * 新闻窗口
     *
     * @param bean
     */
    private void NewWindow(final List<NewsBean> bean) {
        wmNews = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpNews = new WindowManager.LayoutParams();
        lpNews.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpNews.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpNews.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpNews.format = PixelFormat.TRANSLUCENT;
        //类型
        lpNews.type = WindowManager.LayoutParams.TYPE_PHONE;

        mNewsView = View.inflate(getApplicationContext(), R.layout.window_news_item, null);
        //取消按钮
        mNewsView.findViewById(R.id.iv_news_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wmNews.removeView(mNewsView);
                mTTs.startRead("已取消");
                isOfflineBNF = false;
            }
        });
        ListView newsListView = (ListView) mNewsView.findViewById(R.id.newsListView);
        NewsAdapter newsAdapter = new NewsAdapter(getApplicationContext(), bean);
        newsListView.setAdapter(newsAdapter);
        wmNews.addView(mNewsView, lpNews);

        //点击事件
        newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(WakeUpService.this, WebViewActivity.class);
                intent.putExtra("news_title", bean.get(i).getTitle());
                intent.putExtra("news_url", bean.get(i).getUrl());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //播放标题
                mTTs.startRead("正在为你搜索：" + bean.get(i).getTitle());
                wmNews.removeView(mNewsView);
                isOfflineBNF = false;
                isNewsTwo = false;
                mUnivoiceHandler.setVoiceSleep();
            }
        });
    }

    /**
     * 窗口加载进度
     */
    private void showProgress() {
        L.i("showProgress");
        wmProgress = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpProgress = new WindowManager.LayoutParams();
        lpProgress.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpProgress.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpProgress.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpProgress.format = PixelFormat.TRANSLUCENT;
        //类型
        lpProgress.type = WindowManager.LayoutParams.TYPE_PHONE;
        mProgressView = View.inflate(getApplicationContext(), R.layout.window_progress_item, null);
        wmProgress.addView(mProgressView, lpProgress);
    }

    /**
     * 显示更多的地点
     *
     * @param bean
     */
    private void showPoiSuggWindow(final List<PoiSuggBean> bean) {
        wmPoiSugg = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpPoiSugg = new WindowManager.LayoutParams();
        lpPoiSugg.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpPoiSugg.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpPoiSugg.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpPoiSugg.format = PixelFormat.TRANSLUCENT;
        //类型
        lpPoiSugg.type = WindowManager.LayoutParams.TYPE_PHONE;
        mPoiView = View.inflate(getApplicationContext(), R.layout.window_poi_item, null);
        //取消按钮
        mPoiView.findViewById(R.id.iv_poi_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wmPoiSugg.removeView(mPoiView);
                mTTs.startRead("已取消");
                isOfflineBNF = false;
                isNavigationTwo = false;
            }
        });
        ListView poiListView = (ListView) mPoiView.findViewById(R.id.poiListView);
        PoiAdapter poiAdapter = new PoiAdapter(getApplicationContext(), bean);
        poiListView.setAdapter(poiAdapter);
        wmPoiSugg.addView(mPoiView, lpPoiSugg);
        //点击事件
        poiListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    FunctionUtils.StartNavigation(getApplicationContext(), bean.get(i).getLa(), bean.get(i).getLo());
                    isOfflineBNF = false;
                    isNavigationTwo = false;
                    mUnivoiceHandler.setVoiceSleep();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                L.i("经纬度：" + bean.get(i).getLa() + ":" + bean.get(i).getLo());
                //播放标题
                mTTs.startRead("正在为你导航到：" + bean.get(i).getName());
                wmPoiSugg.removeView(mPoiView);
            }
        });
    }

    /**
     * 显示一行文字
     *
     * @param text
     */
    private void showTextView(String text) {
        wmText = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpText = new WindowManager.LayoutParams();
        lpText.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpText.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpText.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpText.format = PixelFormat.TRANSLUCENT;
        //类型
        lpText.type = WindowManager.LayoutParams.TYPE_PHONE;

        mTextView = View.inflate(getApplicationContext(), R.layout.window_text_item, null);

        //取消按钮
        mTextView.findViewById(R.id.iv_text_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wmText.removeView(mTextView);
                mTTs.startRead("已取消");
            }
        });
        //设置天气
        TextView tvText = (TextView) mTextView.findViewById(R.id.tvText);
        tvText.setText(text);
        //显示天气
        wmText.addView(mTextView, lpText);
    }

    /**
     * 显示天气
     *
     * @param city      城市
     * @param weather   天气
     * @param wind      风力
     * @param direction 风向
     * @param t1        最高温
     * @param t2        最低温
     * @param time      时间
     */
    private void showWeatherWindow(String city, String weather, String wind, String direction, String t1, String t2, String time) {
        wmWeather = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpWeather = new WindowManager.LayoutParams();
        lpWeather.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpWeather.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpWeather.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpWeather.format = PixelFormat.TRANSLUCENT;
        //类型
        lpWeather.type = WindowManager.LayoutParams.TYPE_PHONE;

        mWeatherView = View.inflate(getApplicationContext(), R.layout.window_weather_item, null);

        //取消按钮
        mWeatherView.findViewById(R.id.iv_weather_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wmWeather.removeView(mWeatherView);
                mTTs.startRead("已取消");
            }
        });
        //设置城市
        TextView tvCity = (TextView) mWeatherView.findViewById(R.id.tvCity);
        tvCity.setText(city);
        //设置天气
        TextView tvWeather = (TextView) mWeatherView.findViewById(R.id.tvWeather);
        tvWeather.setText("天气：" + weather);
        //设置风力
        TextView tvWind = (TextView) mWeatherView.findViewById(R.id.tvWind);
        tvWind.setText("风力：" + wind);
        //设置风向
        TextView tvDirection = (TextView) mWeatherView.findViewById(R.id.tvDirection);
        tvDirection.setText("风向：" + direction);
        //设置温差
        TextView tvDifference = (TextView) mWeatherView.findViewById(R.id.tvDifference);
        tvDifference.setText("温差:" + t2 + "℃ - " + t1 + "℃");

        //设置图标
        ImageView ivIcon = (ImageView) mWeatherView.findViewById(R.id.ivWeatherIcon);
        if (weather.equals("晴")) {
            ivIcon.setBackgroundResource(R.drawable.w0);
        } else if (weather.equals("小雨")) {
            ivIcon.setBackgroundResource(R.drawable.w7);
        } else if (weather.equals("大雨")) {
            ivIcon.setBackgroundResource(R.drawable.w10);
        } else if (weather.equals("雷阵雨")) {
            ivIcon.setBackgroundResource(R.drawable.w4);
        } else if (weather.equals("多云")) {
            ivIcon.setBackgroundResource(R.drawable.w2);
        } else if (weather.equals("阵雨")) {
            ivIcon.setBackgroundResource(R.drawable.w7);
        } else {
            ivIcon.setBackgroundResource(R.drawable.w7);
        }

        mTTs.startRead(city + time + "的天气" + weather + "," + direction + "风力：" + wind + "最高温度：" + t1 + "最低温度：" + t2);

        //显示天气
        wmWeather.addView(mWeatherView, lpWeather);
        isWeatherHide = true;
    }

    /**
     * 更新唤醒词的广播
     */
    public class NewWakeUpReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();
            final String newWakeup = intent.getStringExtra("new_wakeup");
            L.i(action + ":" + newWakeup);
            showProgress();
            if (StaticClass.NEW_WAKEUP_ACTION_BROADCAST.equals(action)) {
                if (!TextUtils.isEmpty(newWakeup)) {
                    //更新唤醒词
                    mUnivoiceHandler.changeNewWakeUpWord(newWakeup, new OnChangeListener() {
                        @Override
                        public void onChangeSuccess() {
                            int code = mUnivoiceHandler.reLoadWakeUpResource();
                            if (code != 0) {
                                if (mProgressView.getParent() != null) {
                                    wmProgress.removeView(mProgressView);
                                }
                                mTTs.startRead("更新唤醒词为：" + newWakeup);
                            } else {
                                mTTs.startRead("唤醒词更新失败");
                                //刷新
                                mUnivoiceHandler.reLoadWakeUpResource();
                            }
                        }

                        @Override
                        public void onChangeFailure(Exception e) {
                            L.i("no ");
                            mTTs.startRead("唤醒词更新失败");
                            //恢复默认
                            SharePreUtils.putString(getApplicationContext(), "wakeup_name", "你好小优");
                        }
                    });
                }
            }
        }
    }

    /**
     * 来电广播
     */
    public class BluetoothCallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String phone = intent.getStringExtra(StaticClass.BLUETOOTH_CALL_IN_ACTION);
            L.i("phone：" + phone + " action:" + action);
            //来电
            if (StaticClass.BLUETOOTH_CALL_IN.equals(action)) {
                if (!TextUtils.isEmpty(phone)) {
                    showCallPhoneWindow(context, phone);
                }
            }
        }
    }

    /**
     * 显示接听还是挂断的
     *
     * @param context
     */
    private void showCallPhoneWindow(final Context context, String phone) {
        mTTs.startRead("来电话了，接听还是挂断");
        L.i("showCallPhoneWindow");
        wmAnswer = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpAnswer = new WindowManager.LayoutParams();
        lpAnswer.width = WindowManager.LayoutParams.MATCH_PARENT;
        lpAnswer.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpAnswer.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpAnswer.format = PixelFormat.TRANSLUCENT;
        //类型
        lpAnswer.type = WindowManager.LayoutParams.TYPE_PHONE;
        mAnswerView = View.inflate(context, R.layout.window_answer_item, null);
        mAnswerView.findViewById(R.id.btnCancal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 从窗口移除布局
                wmAnswer.removeView(mAnswerView);
                mTTs.startRead("已取消");
                //挂断电话
                //FunctionUtils.endCall(context);
            }
        });

        TextView tvPhone = (TextView) mAnswerView.findViewById(R.id.tvPhone);
        tvPhone.setText("来电号码：" + phone);

        //接听电话
        mAnswerView.findViewById(R.id.btnAnswer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wmAnswer.removeView(mAnswerView);
                //接听
                // Telephony.answerRingingCall(getApplicationContext());
            }
        });
        wmAnswer.addView(mAnswerView, lpAnswer);
        L.i("显示来电接听/挂断");
    }

    /**
     * 播放模式
     *
     * @param playModel
     */
    private void setPlayModel(int playModel) {
        L.i("播放模式：" + playModel);
        switch (playModel) {
            case 0:
                //循环播放
                mKwapi.setPlayMode(getApplicationContext(), PlayMode.MODE_ALL_CIRCLE);
                break;
            case 1:
                //单曲循环
                mKwapi.setPlayMode(getApplicationContext(), PlayMode.MODE_SINGLE_CIRCLE);
                break;
            case 2:
                //顺序播放
                mKwapi.setPlayMode(getApplicationContext(), PlayMode.MODE_ALL_ORDER);
                break;
            case 3:
                //随机播放
                mKwapi.setPlayMode(getApplicationContext(), PlayMode.MODE_ALL_RANDOM);
                break;
            case 4:
                //播放
                mKwapi.setPlayState(getApplicationContext(), PlayState.STATE_PLAY);
                break;
            case 5:
                //暂停
                mKwapi.setPlayState(getApplicationContext(), PlayState.STATE_PAUSE);
                break;
            case 6:
                //上一个
                mKwapi.setPlayState(getApplicationContext(), PlayState.STATE_PRE);
                break;
            case 7:
                //下一个
                mKwapi.setPlayState(getApplicationContext(), PlayState.STATE_NEXT);
                break;

            /**
             * 完成的二次流程：
             * 第一次：7 8 15 16 17 19 1 2 3 4 5 6 18 7 8 9 11 14 17 19 20 21 22 7 8 9 11 14 17 19
             * 未发送bug
             * 第二次：7 8 15 16 17 19 1 2 3 4 5 6 18 7 8 9 11 14 17 19 20 21 22 7 8 9 11 14 17 19
             * 未发现bug
             * 错误：去东方明珠
             *        7 8 15 16 17 19 1 2 3 4 5 6 18 7 8 9 11 14 17 19 20 21 22
             *
             */

        }
    }

    /**
     * 显示测试弹窗
     */
    private void showTestWindow() {
        WindowManager wmTest = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        WindowManager.LayoutParams lpTest = new WindowManager.LayoutParams();
        lpTest.width = 350;
        lpTest.height = 200;
        lpTest.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        lpTest.format = PixelFormat.TRANSLUCENT;
        lpTest.gravity = Gravity.RIGHT | Gravity.TOP;
        //类型
        lpTest.type = WindowManager.LayoutParams.TYPE_PHONE;
        View mTestView = View.inflate(getApplicationContext(), R.layout.window_test_item, null);
        testScrollView = (ScrollView) mTestView.findViewById(R.id.testScrollView);
        tvLog = (TextView) mTestView.findViewById(R.id.tvLog);
        tvLog.append("测试模式启动...！\n");
        wmTest.addView(mTestView, lpTest);

    }
}
