package com.okl.okl_voice.service;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.service
 *  文件名:   DowloadService
 *  创建者:   LGL
 *  创建时间:  2016/8/5 15:54
 *  描述：    下载服务
 */

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * 用于软件更新下载的服务，暂时是直接下载，后期可能需要后台下载的时候用到
 */
public class DowloadService extends Service{

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
