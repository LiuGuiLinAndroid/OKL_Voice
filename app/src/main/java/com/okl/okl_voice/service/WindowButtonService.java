package com.okl.okl_voice.service;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.service
 *  文件名:   WindowButtonService
 *  创建者:   LGL
 *  创建时间:  2016/8/3 17:41
 *  描述：    窗口按钮服务
 */

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.utils.SharePreUtils;

public class WindowButtonService extends Service {

    //窗口管理器
    private WindowManager wm;
    //view
    private View mView;
    //起始坐标
    private int startX, startY;
    //终点坐标
    private int endX, endY;
    //偏移量
    private int dx, dy;
    //窗口图片
    private ImageView iv_mic;
    //布局 参数
    private WindowManager.LayoutParams layoutParams;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        L.i("WindowButtonService———onCreate");
        showWindow();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        L.i("WindowButtonService———onStartCommand");

        //加载view
        wm.addView(mView, layoutParams);

        return START_STICKY;
    }

    /**
     * 显示窗口
     */
    private void showWindow() {
        //窗口管理器
        wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        //布局参数
        layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                //WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | 不能触摸
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        //格式
        layoutParams.format = PixelFormat.TRANSLUCENT;
        //类型
        layoutParams.type = WindowManager.LayoutParams.TYPE_PHONE;

        mView = View.inflate(getApplicationContext(), R.layout.layout_window_button_item, null);
        iv_mic = (ImageView) mView.findViewById(R.id.iv_mic);

        //设置坐标
        int lastX = SharePreUtils.getInt(getApplicationContext(), StaticClass.LAST_X, 0);
        int lastY = SharePreUtils.getInt(getApplicationContext(), StaticClass.LAST_Y, 0);
        layoutParams.x = lastX;
        layoutParams.y = lastY;

        //触摸事件
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        iv_mic.setBackgroundResource(R.drawable.home_mic_pressed);

                        startX = (int) motionEvent.getRawX();
                        startY = (int) motionEvent.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        endX = (int) motionEvent.getRawX();
                        endY = (int) motionEvent.getRawY();

                        //计算移动偏移量
                        dx = endX - startX;
                        dy = endY - startY;

                        /**
                         *根据偏移量更新位置(重新部署位置)
                         */
                        layoutParams.x += dx;
                        layoutParams.y += dy;

                        //更新位置
                        wm.updateViewLayout(mView, layoutParams);

                        //重新初始化坐标
                        startX = (int) motionEvent.getRawX();
                        startY = (int) motionEvent.getRawY();
                        break;
                    case MotionEvent.ACTION_UP:

                        iv_mic.setBackgroundResource(R.drawable.home_mic_normal);

                        //保存坐标
                        SharePreUtils.putInt(getApplicationContext(), StaticClass.LAST_X, layoutParams.x);
                        SharePreUtils.putInt(getApplicationContext(), StaticClass.LAST_Y, layoutParams.y);
                        break;
                }
                return false;

            }
        });


        //点击事件
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //启动这个服务
                // startService(new Intent(getApplicationContext(), WindowService.class));
            }
        });
    }

    //销毁
    @Override
    public void onDestroy() {
        L.i("WindowButtonService———onDestroy");
        super.onDestroy();
        if (wm != null && mView != null) {
            wm.removeView(mView);
        }
    }

}
