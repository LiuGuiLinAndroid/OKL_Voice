package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   VersionUpdateActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/3 15:19
 *  描述：    版本升级
 */

import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kymjs.rxvolley.RxVolley;
import com.kymjs.rxvolley.client.HttpCallback;
import com.kymjs.rxvolley.client.ProgressListener;
import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.view.CustomDialog;

import org.json.JSONException;
import org.json.JSONObject;

public class VersionUpdateActivity extends BaseActivity implements View.OnClickListener {

    //当前版本号
    private TextView tvVersion;
    //是否升级
    private TextView tvisUpdate;
    //升级提示框
    private CustomDialog dialog_update;
    //下载地址
    private String dowload_url;
    //提示框两个按钮
    private Button btnUpdate, btnCancal;
    //更新内容
    private TextView tv_update_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_update);

        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {
        dialog_update = new CustomDialog(this, 0, 0, R.layout.dialog_update_app, R.style.Theme_dialog, Gravity.CENTER, 0);
        btnUpdate = (Button) dialog_update.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);
        btnCancal = (Button) dialog_update.findViewById(R.id.btnCancal);
        btnCancal.setOnClickListener(this);
        tv_update_content = (TextView) dialog_update.findViewById(R.id.tv_update_content);

        tvVersion = (TextView) findViewById(R.id.tvVersion);
        //设置版本号
        tvVersion.setText("OKL Voice:" + FunctionUtils.getAppVersionName(this));
        tvisUpdate = (TextView) findViewById(R.id.tvisUpdate);
        tvisUpdate.setText(getString(R.string.now_latest_version));
        tvisUpdate.setOnClickListener(this);
    }

    /**
     * 是否升级
     */
    private void isUpdate() {
        /**
         * 请求OSS配置文件，如果有，就弹框，点击确定，就开始升级
         */
        RxVolley.get(StaticClass.UPDATE_URL, new HttpCallback() {
            @Override
            public void onSuccess(String t) {
                /**
                 * 返回的json，我们应该定义的以下几个字段
                 * versionName:版本号
                 * versionCode：升级码
                 * content：更新内容
                 * url：apk下载地址
                 */
                parsingJson(t);
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                L.i("检测失败");
            }
        });
    }

    /**
     * 解析检测升级的json
     *
     * @param json
     */
    private void parsingJson(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            //判断是否更新
            int versionCode = jsonObject.getInt("versionCode");
            //服务器版本 > 当前版本
            if (versionCode > FunctionUtils.getAppVersionCode(this)) {
                dowload_url = jsonObject.getString("url");
                tv_update_content.setText(jsonObject.getString("content"));
                //提示框
                dialog_update.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnUpdate:
                //可以升级
                dowloadApk();
                break;
            case R.id.btnCancal:
                dialog_update.dismiss();
                tvisUpdate.setText(getString(R.string.have_update_app));
                break;
            case R.id.tvisUpdate:
                if (tvisUpdate.getText().toString().equals(getString(R.string.have_update_app))) {
                    // dowloadApk();
                    //直接下载还是提示框呢？
                    dialog_update.show();
                }
                break;
        }
    }

    /**
     * 下载apk安装
     */
    private void dowloadApk() {
        //下载文件
        RxVolley.download(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), dowload_url, new ProgressListener() {
            /**
             * 下载进度回调
             * @param transferredBytes 下载进度
             * @param totalSize 大小
             */
            @Override
            public void onProgress(long transferredBytes, long totalSize) {
                //进度

            }
        }, new HttpCallback() {
            @Override
            public void onSuccess(String t) {
                L.i("下载成功");
                //启动这个apk进行安装

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                L.i("下载失败");
            }
        });
    }
}
