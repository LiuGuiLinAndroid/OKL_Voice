package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   MainSettingActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/3 15:02
 *  描述：    设置
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.SharePreUtils;

import java.util.ArrayList;
import java.util.List;

public class MainSettingActivity extends BaseActivity implements View.OnClickListener {

    //选项卡
    private ViewPager mViewpager;
    //两个View
    private View view1, view2;
    //容器
    private List<View> mList = new ArrayList<>();
    //小圆点
    private ImageView point1, point2;
    //入口
    private LinearLayout ll_update, ll_adout_we;
    //窗口开关
    private ImageView iv_sele_up;
    //窗口开关文字
    private TextView isOnOff;
    //是否唤醒
    private ImageView iv_sele_wake_up;
    //唤醒词状态
    private TextView tvWakeStatus;
    //较慢，标准，较快
    private ImageView the_slower, the_standard, the_faster;
    //当前语速状态
    private TextView tvSpeakStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_setting);

        initView();
    }

    //初始化View
    private void initView() {
        point1 = (ImageView) findViewById(R.id.point1);
        point1.setBackgroundResource(R.drawable.icon_dot_selected);
        point2 = (ImageView) findViewById(R.id.point2);
        point2.setBackgroundResource(R.drawable.icon_dot_normal);

        mViewpager = (ViewPager) findViewById(R.id.mViewpager);

        //第一页
        view1 = View.inflate(this, R.layout.layout_main_setting_one, null);
        iv_sele_wake_up = (ImageView) view1.findViewById(R.id.iv_sele_wake_up);
        iv_sele_wake_up.setOnClickListener(this);
        tvWakeStatus = (TextView) view1.findViewById(R.id.tvWakeStatus);
        boolean isBooleanUp = SharePreUtils.getBoolean(this, StaticClass.WAKE_UP_STATUS, true);
        iv_sele_wake_up.setSelected(isBooleanUp);
        if (isBooleanUp) {
            tvWakeStatus.setText(getString(R.string.hello_cube_setting));
        } else {
            tvWakeStatus.setText(getString(R.string.the_off));
        }
        the_slower = (ImageView) view1.findViewById(R.id.the_slower);
        the_slower.setOnClickListener(this);
        the_standard = (ImageView) view1.findViewById(R.id.the_standard);
        the_standard.setOnClickListener(this);
        //默认普通
        the_standard.setSelected(true);
        the_faster = (ImageView) view1.findViewById(R.id.the_faster);
        the_faster.setOnClickListener(this);
        tvSpeakStatus = (TextView) view1.findViewById(R.id.tvSpeakStatus);

        //第二页
        view2 = View.inflate(this, R.layout.layout_main_setting_two, null);
        iv_sele_up = (ImageView) view2.findViewById(R.id.iv_sele_up);
        iv_sele_up.setOnClickListener(this);
        isOnOff = (TextView) view2.findViewById(R.id.isOnOff);

        boolean isBoolean = SharePreUtils.getBoolean(this, StaticClass.IS_WINDOW_BUTTON, true);
        iv_sele_up.setSelected(isBoolean);
        if (isBoolean) {
            isOnOff.setText(getString(R.string.the_on));
        } else {
            isOnOff.setText(getString(R.string.the_off));
        }
        ll_update = (LinearLayout) view2.findViewById(R.id.ll_update);
        ll_update.setOnClickListener(this);
        ll_adout_we = (LinearLayout) view2.findViewById(R.id.ll_adout_we);
        ll_adout_we.setOnClickListener(this);

        mList.add(view1);
        mList.add(view2);

        //设置数据源
        mViewpager.setAdapter(new MainSettingPagerAdapter());

        //监听事件
        mViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            //切换时状态变换
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    point1.setBackgroundResource(R.drawable.icon_dot_selected);
                    point2.setBackgroundResource(R.drawable.icon_dot_normal);
                } else if (position == 1) {
                    point1.setBackgroundResource(R.drawable.icon_dot_normal);
                    point2.setBackgroundResource(R.drawable.icon_dot_selected);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //版本更新
            case R.id.ll_update:
                startActivity(new Intent(this, VersionUpdateActivity.class));
                break;
            //关于我们
            case R.id.ll_adout_we:
                startActivity(new Intent(this, AboutWeActivity.class));
                break;
            //是否打开窗口
            case R.id.iv_sele_up:
                iv_sele_up.setSelected(!iv_sele_up.isSelected());
                //开启状态
                if (iv_sele_up.isSelected()) {
                    SharePreUtils.putBoolean(this, StaticClass.IS_WINDOW_BUTTON, true);
                    isOnOff.setText(getString(R.string.the_on));
                } else {
                    isOnOff.setText(getString(R.string.the_off));
                    SharePreUtils.putBoolean(this, StaticClass.IS_WINDOW_BUTTON,false);
                }
                break;
            //是否开启唤醒词
            case R.id.iv_sele_wake_up:
                iv_sele_wake_up.setSelected(!iv_sele_wake_up.isSelected());
                //开启状态
                if (iv_sele_wake_up.isSelected()) {
                    tvWakeStatus.setText(getString(R.string.hello_cube_setting));

                } else {
                    tvWakeStatus.setText(getString(R.string.the_off));
                }
                break;
            //慢
            case R.id.the_slower:
                SharePreUtils.putInt(this,StaticClass.SPEAK_SPEED,50);
                the_slower.setSelected(!the_slower.isSelected());
                if(the_slower.isSelected()){
                    tvSpeakStatus.setText(getString(R.string.the_slower));
                    the_standard.setSelected(false);
                    the_faster.setSelected(false);
                }
                break;
            //普通
            case R.id.the_standard:
                SharePreUtils.putInt(this,StaticClass.SPEAK_SPEED,100);
                the_standard.setSelected(!the_standard.isSelected());
                if(the_standard.isSelected()){
                    tvSpeakStatus.setText(getString(R.string.the_standard));
                    the_slower.setSelected(false);
                    the_faster.setSelected(false);
                }
                break;
            //快
            case R.id.the_faster:
                SharePreUtils.putInt(this,StaticClass.SPEAK_SPEED,200);
                the_faster.setSelected(!the_faster.isSelected());
                if(the_faster.isSelected()){
                    tvSpeakStatus.setText(getString(R.string.the_faster));
                    the_standard.setSelected(false);
                    the_slower.setSelected(false);
                }
                break;

        }
    }

    /**
     * 数据源
     */
    class MainSettingPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mList.get(position));
            super.destroyItem(container, position, object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(mList.get(position));
            return mList.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharePreUtils.putBoolean(this, StaticClass.WAKE_UP_STATUS, iv_sele_wake_up.isSelected());
    }
}
