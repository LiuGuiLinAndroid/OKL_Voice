package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   WebViewActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/12 12:41
 *  描述：    网页浏览Activity
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.okl.okl_voice.R;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.utils.L;

public class WebViewActivity extends BaseActivity implements View.OnClickListener{

    //新闻标题
    private String newsTitle;
    //新闻url
    private String newsUrl;
    //加载进度
    private ProgressBar pb;
    //浏览器
    private WebView webView;
    //返回主页
    private FloatingActionButton fb_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        initView();
    }

    //初始化View
    private void initView() {
        fb_home = (FloatingActionButton) findViewById(R.id.fb_home);
        fb_home.setOnClickListener(this);
        Intent intent = getIntent();
        newsTitle = intent.getStringExtra("news_title");
        newsUrl = intent.getStringExtra("news_url");

       if(!TextUtils.isEmpty(newsTitle)){
           getSupportActionBar().setTitle(newsTitle);
       }

        L.i("title:" + newsTitle + "url:" + newsUrl);

        pb = (ProgressBar) findViewById(R.id.pb);
        pb.setMax(100);
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebChromeClient(new WebViewClient());
        webView.loadUrl(newsUrl);

        //本地显示
        webView.setWebViewClient(new android.webkit.WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fb_home:
                //返回主页
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
                FunctionUtils.sendTTSBro(this,"返回主页");
                break;
        }
    }

    /**
     * 接口回调
     */
    public class WebViewClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            pb.setProgress(newProgress);
            if (newProgress == 100) {
                pb.setVisibility(View.GONE);
            }
            super.onProgressChanged(view, newProgress);
        }
    }
}
