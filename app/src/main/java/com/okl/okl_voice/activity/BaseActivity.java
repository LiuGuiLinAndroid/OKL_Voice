package com.okl.okl_voice.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   BaseActivity
 *  创建者:   LGL
 *  创建时间:  2016/7/5 15:12
 *  描述：    Activity的基类,共性方法
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar bar = getSupportActionBar();
        //标题显示返回按钮
        bar.setDisplayHomeAsUpEnabled(true);
        //设置背景图片
        //bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_bg));

    }

    /**
     * 菜单操作
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
