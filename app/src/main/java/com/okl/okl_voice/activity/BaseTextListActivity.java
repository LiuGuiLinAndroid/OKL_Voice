package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   BaseTextListActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/6 10:34
 *  描述：    全部功能-各详情的基类
 */

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.adapter.BaseTextAdapter;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.L;

import java.util.ArrayList;
import java.util.List;

public class BaseTextListActivity extends BaseActivity {

    //列表
    private ListView list_call_phone, list_get_phone, list_send_sms;
    //数据源
    private BaseTextAdapter mCallAdapter, mGetAdapter, mSendSmsAdapter;
    //数据
    private List<String> mListCall = new ArrayList<>();
    private List<String> mListGet = new ArrayList<>();
    private List<String> mListSendSms = new ArrayList<>();
    //数据源
    private BaseTextAdapter textAdapter;
    //数据
    private List<String> mList = new ArrayList<>();
    //列表
    private ListView list_text;
    //标记
    private int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //判断是哪一个Activity
        Intent intent = getIntent();
        flag = intent.getIntExtra(StaticClass.ACTIVITY_FLAG, 1);
        L.i("flag = " + flag);

        initData();
        initView();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        switch (flag) {
            case 0:
                setContentView(R.layout.activity_bluetooth);
                getSupportActionBar().setTitle(R.string.bluetooth);
                mListCall.add(getString(R.string.bluetooth_date_1));
                mListCall.add(getString(R.string.bluetooth_date_2));
                mListCall.add(getString(R.string.bluetooth_date_3));
                mListCall.add(getString(R.string.bluetooth_date_4));

                mListGet.add(getString(R.string.bluetooth_date_5));
                mListGet.add(getString(R.string.bluetooth_date_6));

                mListSendSms.add(getString(R.string.bluetooth_date_7));
                mListSendSms.add(getString(R.string.bluetooth_date_8));
                break;
            case 1:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.navigation);
                mList.add(getString(R.string.navigation_data_1));
                mList.add(getString(R.string.navigation_data_2));
                mList.add(getString(R.string.navigation_data_3));
                break;
            case 2:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.music);
                mList.add(getString(R.string.music_data_1));
                mList.add(getString(R.string.music_data_2));
                mList.add(getString(R.string.music_data_3));
                mList.add(getString(R.string.music_data_4));
                mList.add(getString(R.string.music_data_5));
                break;
            case 3:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.setting);
                mList.add(getString(R.string.setting_data_1));
                mList.add(getString(R.string.setting_data_2));
                mList.add(getString(R.string.setting_data_3));
                mList.add(getString(R.string.setting_data_4));
                break;
            case 4:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.radio);
                mList.add(getString(R.string.radio_data_1));
                mList.add(getString(R.string.radio_data_2));
                mList.add(getString(R.string.radio_data_3));
                mList.add(getString(R.string.radio_data_4));
                mList.add(getString(R.string.radio_data_5));
                mList.add(getString(R.string.radio_data_6));
                break;
            case 5:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.weather);
                mList.add(getString(R.string.weather_data_1));
                mList.add(getString(R.string.weather_data_2));
                break;
            case 6:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.stock);
                mList.add(getString(R.string.stock_data_1));
                mList.add(getString(R.string.stock_data_2));
                break;
            case 7:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.local);
                mList.add(getString(R.string.local_data_1));
                mList.add(getString(R.string.local_data_2));
                mList.add(getString(R.string.local_data_3));
                break;
            case 8:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.traffic);
                mList.add(getString(R.string.traffic_data_1));
                mList.add(getString(R.string.traffic_data_2));
                mList.add(getString(R.string.traffic_data_3));
                break;
            case 9:
                setContentView(R.layout.activity_base_text_list);
                getSupportActionBar().setTitle(R.string.limit);
                mList.add(getString(R.string.limit_data_1));
                mList.add(getString(R.string.limit_data_2));
                mList.add(getString(R.string.limit_data_3));
                break;
        }
    }

    /**
     * 初始化数据
     */
    private void initView() {
        if (flag == 0) {
            //一级
            list_call_phone = (ListView) findViewById(R.id.list_call_phone);
            mCallAdapter = new BaseTextAdapter(this, mListCall);
            list_call_phone.setAdapter(mCallAdapter);
            //二级
            list_get_phone = (ListView) findViewById(R.id.list_get_phone);
            mGetAdapter = new BaseTextAdapter(this, mListGet);
            list_get_phone.setAdapter(mGetAdapter);
            //三级
            list_send_sms = (ListView) findViewById(R.id.list_send_sms);
            mSendSmsAdapter = new BaseTextAdapter(this, mListSendSms);
            list_send_sms.setAdapter(mSendSmsAdapter);
        } else {
            list_text = (ListView) findViewById(R.id.list_text);
            textAdapter = new BaseTextAdapter(this, mList);
            list_text.setAdapter(textAdapter);
        }

    }
}
