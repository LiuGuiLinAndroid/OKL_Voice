package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   AboutWeActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/3 15:45
 *  描述：    关于我们
 */

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.utils.FunctionUtils;

public class AboutWeActivity extends BaseActivity implements View.OnClickListener {

    //公司名
    private TextView companyName;
    //官网
    private TextView companyUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_we);

        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {
        companyName = (TextView) findViewById(R.id.companyName);
        companyUrl = (TextView) findViewById(R.id.companyUrl);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //设置字体
        FunctionUtils.setTextViewFont(this, companyName);
        FunctionUtils.setTextViewFont(this, companyUrl);
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {

    }
}
