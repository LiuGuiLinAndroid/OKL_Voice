package com.okl.okl_voice.activity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   AllFunctionActivity
 *  创建者:   LGL
 *  创建时间:  2016/8/3 14:17
 *  描述：    全部功能
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;

import java.util.ArrayList;
import java.util.List;

public class AllFunctionActivity extends BaseActivity implements View.OnClickListener {

    //选项卡
    private ViewPager mViewpager;
    //两个View
    private View view1, view2;
    //容器
    private List<View> mList = new ArrayList<>();
    //小圆点
    private ImageView point1, point2;
    //入口
    private LinearLayout ll_bluetooth, ll_navigation, ll_music, ll_setting, ll_radio, ll_weather, ll_stock, ll_local, ll_traffic, ll_limit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allfunction);

        initView();
    }

    //初始化View
    private void initView() {
        point1 = (ImageView) findViewById(R.id.point1);
        point1.setBackgroundResource(R.drawable.icon_dot_selected);
        point2 = (ImageView) findViewById(R.id.point2);
        point2.setBackgroundResource(R.drawable.icon_dot_normal);

        mViewpager = (ViewPager) findViewById(R.id.mViewpager);

        view1 = View.inflate(this, R.layout.layout_all_function_one, null);
        ll_bluetooth = (LinearLayout) view1.findViewById(R.id.ll_bluetooth);
        ll_bluetooth.setOnClickListener(this);
        ll_navigation = (LinearLayout) view1.findViewById(R.id.ll_navigation);
        ll_navigation.setOnClickListener(this);
        ll_music = (LinearLayout) view1.findViewById(R.id.ll_music);
        ll_music.setOnClickListener(this);
        ll_setting = (LinearLayout) view1.findViewById(R.id.ll_setting);
        ll_setting.setOnClickListener(this);
        ll_radio = (LinearLayout) view1.findViewById(R.id.ll_radio);
        ll_radio.setOnClickListener(this);
        ll_weather = (LinearLayout) view1.findViewById(R.id.ll_weather);
        ll_weather.setOnClickListener(this);
        ll_stock = (LinearLayout) view1.findViewById(R.id.ll_stock);
        ll_stock.setOnClickListener(this);
        ll_local = (LinearLayout) view1.findViewById(R.id.ll_local);
        ll_local.setOnClickListener(this);

        view2 = View.inflate(this, R.layout.layout_all_function_two, null);
        ll_traffic = (LinearLayout) view2.findViewById(R.id.ll_traffic);
        ll_traffic.setOnClickListener(this);
        ll_limit = (LinearLayout) view2.findViewById(R.id.ll_limit);
        ll_limit.setOnClickListener(this);

        mList.add(view1);
        mList.add(view2);

        //设置数据源
        mViewpager.setAdapter(new AllFunctionPagerAdapter());

        //监听事件
        mViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            //切换时状态变换
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    point1.setBackgroundResource(R.drawable.icon_dot_selected);
                    point2.setBackgroundResource(R.drawable.icon_dot_normal);
                } else if (position == 1) {
                    point1.setBackgroundResource(R.drawable.icon_dot_normal);
                    point2.setBackgroundResource(R.drawable.icon_dot_selected);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, BaseTextListActivity.class);
        switch (view.getId()) {
            //蓝牙通讯
            case R.id.ll_bluetooth:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 0);
                break;
            //导航
            case R.id.ll_navigation:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 1);
                break;
            //听音乐
            case R.id.ll_music:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 2);
                break;
            //快捷控制
            case R.id.ll_setting:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 3);
                break;
            //收音机
            case R.id.ll_radio:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 4);
                break;
            //天气实况
            case R.id.ll_weather:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 5);
                break;
            //股市行情
            case R.id.ll_stock:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 6);
                break;
            //周边查询
            case R.id.ll_local:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 7);
                break;
            //实时路况
            case R.id.ll_traffic:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 8);
                break;
            //尾号限行
            case R.id.ll_limit:
                i.putExtra(StaticClass.ACTIVITY_FLAG, 9);
                break;
        }
        //跳转
        startActivity(i);
    }

    /**
     * 数据源
     */
    class AllFunctionPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mList.get(position));
            super.destroyItem(container, position, object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(mList.get(position));
            return mList.get(position);
        }
    }
}
