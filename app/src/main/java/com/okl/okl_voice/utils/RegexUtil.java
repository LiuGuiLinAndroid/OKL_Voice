package com.okl.okl_voice.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by LGL on 2016/8/9.
 */
public class RegexUtil {

    public static String getChineseWord(String text){
        String rex = "[\\d+[\\u4E00-\\u9FA5]+]*";
        StringBuffer sb = new StringBuffer();
        Pattern p = Pattern.compile(rex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            sb.append(m.group(0));
        }
        return sb.toString();
    }
}
