package com.okl.okl_voice.utils;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.utils
 *  文件名:   TTSUtils
 *  创建者:   LGL
 *  创建时间:  2016/8/8 11:36
 *  描述：    TTS工具类
 */

import android.content.Context;
import android.os.Handler;

import com.okl.okl_voice.entity.StaticClass;
import com.uvoice.jhb.univoicettssdk.UniVoiceTTS;



public class TTSUtils {

    //TTS
    private static UniVoiceTTS mTTs;
    //单例
    private static TTSUtils instance;

    /**
     * 不对外的构造函数
     */
    private TTSUtils() {

    }

    /**
     * 单例模式
     *
     * @return
     */
    public static synchronized TTSUtils getInstance(Context mContext,Handler handler) {
        if (instance == null) {
            instance = new TTSUtils();
            init(mContext,handler);
        }
        return instance;
    }

    /**
     * 初始化
     */
    public static void init(Context mContext, Handler handler) {
        mTTs = new UniVoiceTTS(mContext,handler);
        int speed = SharePreUtils.getInt(mContext, StaticClass.SPEAK_SPEED,100);
        L.i("语速：" + speed);
        //设置朗读的速度，默认100
        mTTs.setRate(speed);
        //设置朗读的声调,，默认108
        mTTs.setPitch(108);
    }

    /**
     * 朗诵
     *
     * @param text
     */
    public void startRead(String text) {
        mTTs.startRead(text);
    }

    /**
     * 停止朗诵
     */
    public void stopRead() {
        mTTs.stopRead();
    }

    /**
     * 销毁TTS
     */
    public void destoryTTs() {
        mTTs.destroyTTS();
    }

}
