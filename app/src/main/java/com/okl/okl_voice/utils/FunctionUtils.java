package com.okl.okl_voice.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.StaticClass;

import java.io.File;
import java.lang.reflect.Method;
import java.util.List;

import cn.kuwo.autosdk.api.KWAPI;
import cn.kuwo.autosdk.api.PlayState;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.utils
 *  文件名:   FunctionUtils
 *  创建者:   LGL
 *  创建时间:  2016/7/5 15:06
 *  描述：    特定功能的工具类
 *            - 打电话
 *            - 打开/关闭 应用
 *            - 增大/减小 音量
 *            - 高德地图导航
 *            - 判断网络连接
 *            - 判断wifi连接
 *            - 打开设置
 *            - 判断应用是否在前台
 *            - 播放歌曲
 *            - 判断运营商
 *            - 说话
 *            - 返回主页
 *            - fm
 */
public class FunctionUtils {

    /**
     * 蓝牙电话本拨打指定电话
     *
     * @param mContext 上下文
     * @param phone    电话号码
     */
    public static void CallPhone(Context mContext, String phone) {
        L.i("电话号码", phone);
        // 广播名
        Intent intent = new Intent();
        intent.setClassName(StaticClass.BLUETOOTH_PACKAGE, StaticClass.BLUETOOTH_PACKAGE_FIRST);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        intent.putExtra("PhoneNumber", phone);
        mContext.startActivity(intent);
        intent = new Intent();
        intent.setAction(StaticClass.BLUETOOTH_CALL);
        intent.putExtra("PhoneNumber", phone);
        mContext.sendBroadcast(intent);
    }

    /**
     * 打开应用
     *
     * @param mContext      上下文
     * @param packName      包名
     * @param firstActivity Activity入口
     */
    public static void StartAPP(Context mContext, String packName, String firstActivity) {
        ComponentName componet = new ComponentName(packName, firstActivity);
        Intent i = new Intent();
        i.setComponent(componet);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }


    /**
     * 关闭应用
     *
     * @param mContext 上下文
     * @param packName 包名
     */
    public static void EndApp(Context mContext, String packName) {
        Intent mIntent = new Intent();
        mIntent.setAction(StaticClass.STOP_APP);
        mIntent.putExtra("packageName", packName);
        mContext.sendBroadcast(mIntent);
    }

    /**
     * 设置系统音量
     *
     * @param mContext 上下文
     * @param flag     0 降低  1 增加
     */
    public static void SetSystemVolume(Context mContext, int flag) {
        AudioManager mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        //最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        //最小音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        // 降低音量，调出系统音量控制
        if (flag == 0) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER,
                    AudioManager.FX_FOCUS_NAVIGATION_UP);
            // 增加音量，调出系统音量控制
        } else if (flag == 1) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE,
                    AudioManager.FX_FOCUS_NAVIGATION_UP);
        }

    }

    /**
     * 高德地图导航
     *
     * @param mContext 上下文
     * @param la       经度
     * @param lo       纬度
     */
    public static void StartNavigation(Context mContext, double la, double lo) throws Exception {
        if (FunctionUtils.isInstallByread("com.autonavi.minimap")) {
            PackageManager pm = mContext.getPackageManager();
            String appName = mContext.getApplicationInfo().loadLabel(pm).toString();
            // 北京地址：39.9110130000，116.4135540000
            Intent intent = new Intent(StaticClass.INTENT_VIEW,
                    android.net.Uri
                            // long lath
                            .parse("androidamap://navi?sourceApplication="
                                    + appName + "&lat=" + la + "&lon=" + lo
                                    + "&dev=0"));
            intent.setPackage(StaticClass.MAP_PACKAGE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        } else {
            Toast.makeText(mContext, "没有找到地图", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * 判断应用是否安装
     *
     * @param packageName
     * @return
     */
    public static boolean isInstallByread(String packageName) {
        return new File("/data/data/" + packageName).exists();
    }

    /**
     * 判断网络是否连接
     *
     * @param context 上下文
     * @return
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null != connectivity) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (null != info && info.isConnected()) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否是wifi连接
     */
    public static boolean isWifi(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return false;
        return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;

    }

    /**
     * 打开系统界面
     */
    public static void openSystemSetting(Context mContext) {
        mContext.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
    }

    /**
     * 打开系统时间界面
     */
    public static void openSystemTime(Context mContext) {
        mContext.startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
    }


    /**
     * 添加联系人
     *
     * @param mContext 上下文
     * @param name     电话
     * @param phone    号码
     */
    public static void NewContent(Context mContext, String name, String phone) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.dir/person");
        intent.setType("vnd.android.cursor.dir/contact");
        intent.setType("vnd.android.cursor.dir/raw_contact");
        // 添加姓名
        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        // 添加手机
        intent.putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
        intent.putExtra(ContactsContract.Intents.Insert.IM_PROTOCOL, ContactsContract.CommonDataKinds.Im.PROTOCOL_QQ);
        intent.putExtra(ContactsContract.Intents.Insert.IM_PROTOCOL, "444255655");
        mContext.startActivity(intent);
    }

    /**
     * 判断应用是否在前台运行
     * &&在前台为true 不在为false
     *
     * @param mContext 上下文
     * @return
     */
    public static boolean isRunningForeground(Context mContext) {
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        @SuppressWarnings("deprecation")
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String currentPackageName = cn.getPackageName();
        if (!TextUtils.isEmpty(currentPackageName)
                && currentPackageName.equals(mContext.getPackageName())) {
            return true;
        }
        return false;
    }

    /**
     * 播放歌曲
     *
     * @param mContext 上下文
     * @param name     歌名
     * @param singer   歌手
     * @param album    专辑
     */
    public static void PlayMusic(Context mContext, String name, String singer, String album) {
        KWAPI mKwapi = null;
        if (mKwapi == null) {
            mKwapi = KWAPI.createKWAPI(mContext, "auto");
        }
        mKwapi.playClientMusics(mContext, name, singer, album);
        mKwapi.setPlayState(mContext, PlayState.STATE_PLAY);
    }

    /**
     * 判断运营商
     *
     * @param mContext 上下文
     */
    public static void isChinaSIM(Context mContext) {
        TelephonyManager telManager = (TelephonyManager) mContext.getSystemService(Activity.TELEPHONY_SERVICE);
        String operator = telManager.getSimOperator();
        L.d("operator：" + operator);
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002")) {
                L.d("中国移动");
            } else if (operator.equals("46001")) {
                L.d("中国联通");
            } else if (operator.equals("46003")) {
                L.d("中国电信");
            }
        }
    }


    /**
     * 获取当前版本号
     *
     * @param mContext
     * @return
     */
    public static String getAppVersionName(Context mContext) {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            String versionName = info.versionName;
            return versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "无法获取版本号";
        }
    }

    /**
     * 获取版本码
     *
     * @param mContext
     * @return
     */
    public static int getAppVersionCode(Context mContext) {
        try {
            PackageManager manager = mContext.getPackageManager();
            PackageInfo info = manager.getPackageInfo(mContext.getPackageName(), 0);
            int versionCode = info.versionCode;
            return versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            //如果获取不到我就不更新
            return Integer.MAX_VALUE;
        }
    }

    /**
     * 设置字体
     *
     * @param mContext
     * @param textView
     */
    public static void setTextViewFont(Context mContext, TextView textView) {
        final Typeface fontFace = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/YYG.TTF");
        textView.setTypeface(fontFace);
    }

    /**
     * 判断服务是否运行
     * true 运行
     *
     * @param mContext
     * @param serviceName 包名.类名
     * @return
     */
    public static boolean isServiceRunning(Context mContext, String serviceName) {
        // 活动管理器
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        // 获取运行的服务,参数表示最多返回的数量
        List<ActivityManager.RunningServiceInfo> runningServices = am.getRunningServices(100);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : runningServices) {
            String className = runningServiceInfo.service.getClassName();
            if (className.equals(serviceName)) {
                // 判断服务是否运行
                return true;
            }
        }
        return false;
    }

    /**
     * 跳转到系统的网络设置界面
     *
     * @param mContext
     */
    public static void toSettingNetWork(Context mContext) {
        Intent intent = null;
        // 先判断当前系统版本
        if (android.os.Build.VERSION.SDK_INT > 10) {  // 3.0以上
            intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
        } else {
            intent = new Intent();
            intent.setClassName("com.android.settings", "com.android.settings.WirelessSettings");
        }
        mContext.startActivity(intent);
    }

    /**
     * tts結束播放声音
     *
     * @param mContext
     */
    public static void playTTsStop(Context mContext) {
        //这里添加两种播放模式，可随意切换
        boolean isReader = false;
        if (isReader) {
            SoundPool sp = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
            sp.load(mContext, R.raw.tts_stop, 1);
            sp.play(1, 1, 1, 0, 0, 1);
            L.i("SoundPool");
        } else {
            MediaPlayer mPlayer = MediaPlayer.create(mContext, R.raw.tts_stop);
            mPlayer.start();
            L.i("MediaPlayer");
        }
    }

    /**
     * 设置系统亮度
     *
     * @param mContext
     * @param isNum    true 增加 false 减少
     * @param i        设置值
     */
    public static void setScreenPlay(Context mContext, boolean isNum, int i) {
        try {
            // 获取系统亮度 返回的亮度值是处于0-255之间的整型数值。
            int systemBrightness = Settings.System.getInt(mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
            L.i("当前亮度：" + systemBrightness);
            if (isNum) {
                // 设置系统亮度
                Settings.System.putInt(mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, systemBrightness + i);
            } else {
                if (systemBrightness < 60) {
                    return;
                } else {
                    // 设置系统亮度
                    Settings.System.putInt(mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, systemBrightness - i);
                }
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送tts播报的广播
     *
     * @param mContext
     * @param tts
     */
    public static void sendTTSBro(Context mContext, String tts) {
        Intent intent = new Intent("com.okl.okl_voice_tts_interface");
        intent.putExtra("tts_content", tts);
        mContext.sendBroadcast(intent);
    }

    /**
     * 返回主页
     *
     * @param mContext
     */
    public static void goHome(Context mContext) {
        //返回主页
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_HOME);
        mContext.startActivity(intent);
    }

    /**
     * FM调频
     *
     * @param mContext 上下文
     * @param type     类型
     * @param freq     频率
     */
    public static void setFM(Context mContext, String type, String freq) {
        Intent intent = new Intent();
        intent.setAction(StaticClass.FM_ACTION_BROADCAST);
        intent.putExtra("type", type);
        intent.putExtra("freq", freq);
        mContext.sendBroadcast(intent);
        L.i("调频:" + freq);
    }

    /**
     * 挂断电话
     *
     * @param mContext
     */
    public static void endCall(Context mContext) {
        L.i("挂断电话!");
        try {
            Class clazz = Class.forName("android.os.ServiceManager");
            Method method = clazz.getMethod("getService", String.class);
            // 静态方法不需要对象,参1是null
            IBinder b = (IBinder) method.invoke(null, mContext.TELEPHONY_SERVICE);
            ITelephony service = ITelephony.Stub.asInterface(b);
            service.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
