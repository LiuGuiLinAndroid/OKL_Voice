package com.okl.okl_voice.utils;

import android.util.Log;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.utils
 *  文件名:   L
 *  创建者:   LGL
 *  创建时间:  2016/7/5 15:42
 *  描述：    Log统一管理类
 */
public class L {

    /**
     * Log:作为一个开发人员信息查询非常重要的一个环节，他分了五个等级
     * Log(TAG,text);  -----> 调试的，调试开关
     *
     */


    private static final String TAG = "OKL_Voice";

    public static boolean DEBUG = true;

    public static void i(String msg) {
        if (DEBUG) {
            Log.i(TAG, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (DEBUG)
            Log.i(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (DEBUG)
            Log.v(tag, msg);
    }

    public static void d(Object obj) {
        if (DEBUG)
            Log.d(TAG, obj.toString());
    }


    public static void d(String tag, String msg) {
        if (DEBUG)
            Log.d(tag, "!--->" + msg);
    }

    public static void d(String msg) {
        if (DEBUG)
            Log.d(TAG, msg);
    }

    public static void d(String tag, Exception e) {
        Log.d(tag, e.toString());
    }

    public static void w(String msg) {
        if (DEBUG)
            Log.w(TAG, msg);
    }

    public static void w(String tag, String string) {
        if (DEBUG)
            Log.w(tag, string);
    }

    public static void e(String msg) {
        if (DEBUG) {
            Log.e(TAG, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (DEBUG)
            Log.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (DEBUG)
            Log.e(tag, msg, tr);
    }

    public static void printStackTrace(Exception e) {
        if (DEBUG)
            Log.e(TAG, e.toString());
    }
}
