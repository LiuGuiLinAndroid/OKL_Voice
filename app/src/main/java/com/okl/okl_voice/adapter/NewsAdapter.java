package com.okl.okl_voice.adapter;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.adapter
 *  文件名:   NewsAdapter
 *  创建者:   LGL
 *  创建时间:  2016/8/12 11:16
 *  描述：    新闻的数据源
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.NewsBean;

import java.util.List;

public class NewsAdapter extends BaseAdapter {

    //上下文
    private Context mContext;
    //布局加载
    private LayoutInflater inflater;
    //数据
    private List<NewsBean> mList;

    //构造函数
    public NewsAdapter(Context mContext, List<NewsBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
        //获取服务
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.news_list_item, null);
            viewHolder.tv_news_title = (TextView) view.findViewById(R.id.tv_news_title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        NewsBean bean = mList.get(i);
        viewHolder.tv_news_title.setText(bean.getTitle());
        return view;
    }


    class ViewHolder {
        private TextView tv_news_title;
    }
}
