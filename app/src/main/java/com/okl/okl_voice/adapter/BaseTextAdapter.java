package com.okl.okl_voice.adapter;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.adapter
 *  文件名:   BaseTextAdapter
 *  创建者:   LGL
 *  创建时间:  2016/8/3 13:28
 *  描述：    显示单Text的Adapter
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.okl.okl_voice.R;

import java.util.List;

public class BaseTextAdapter extends BaseAdapter {

    //数据
    private List<String> mList;
    //上下文
    private Context mContext;
    //布局加载器
    private LayoutInflater inflater;

    //构造方法
    public BaseTextAdapter(Context mContext, List<String> mList) {
        this.mContext = mContext;
        this.mList = mList;
        //获取系统服务
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {

        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            viewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.base_text_item, null);
            viewHolder.tvText = (TextView) view.findViewById(R.id.tvText);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        //设置数据
        viewHolder.tvText.setText(mList.get(i));
        return view;
    }

    //缓存优化
    static class ViewHolder {
        private TextView tvText;
    }
}
