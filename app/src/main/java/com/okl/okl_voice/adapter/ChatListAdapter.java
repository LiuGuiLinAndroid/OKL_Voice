package com.okl.okl_voice.adapter;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.adapter
 *  文件名:   ChatListAdapter
 *  创建者:   LGL
 *  创建时间:  2016/8/4 13:17
 *  描述：    对话列表Adapter
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.okl.okl_voice.R;
import com.okl.okl_voice.entity.ChatListBean;

import java.util.List;

public class ChatListAdapter extends BaseAdapter {

    //type
    public static final int VALUE_LEFT_TEXT = 1;
    public static final int VALUE_RIGHT_TEXT = 2;

    //布局加载器
    private LayoutInflater mInflater;
    private List<ChatListBean> myList;

    /**
     * 构造方法
     *
     * @param context
     * @param myList
     */
    public ChatListAdapter(Context context, List<ChatListBean> myList) {
        this.myList = myList;
        //获取系统服务
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {

        ChatListBean msg = myList.get(position);
        int type = getItemViewType(position);

        ViewHolderRightText holderRightText = null;
        ViewHolderLeftText holderLeftText = null;
        if (convertView == null) {
            switch (type) {
                // 左边
                case VALUE_LEFT_TEXT:
                    holderLeftText = new ViewHolderLeftText();
                    convertView = mInflater.inflate(R.layout.chatlist_left, null);
                    holderLeftText.btnLeftText = (TextView) convertView
                            .findViewById(R.id.chatlist_tv_left);
                    holderLeftText.btnLeftText.setText(msg.getValue());
                    convertView.setTag(holderLeftText);
                    break;
                // 右边
                case VALUE_RIGHT_TEXT:
                    holderRightText = new ViewHolderRightText();
                    convertView = mInflater.inflate(R.layout.chatlist_right, null);
                    holderRightText.btnRightText = (TextView) convertView
                            .findViewById(R.id.chatlist_tv_right);
                    holderRightText.btnRightText.setText(msg.getValue());
                    convertView.setTag(holderRightText);
                    break;
            }
        } else {
            switch (type) {
                case VALUE_LEFT_TEXT:
                    holderLeftText = (ViewHolderLeftText) convertView.getTag();
                    holderLeftText.btnLeftText.setText(msg.getValue());
                    break;
                case VALUE_RIGHT_TEXT:
                    holderRightText = (ViewHolderRightText) convertView.getTag();
                    holderRightText.btnRightText.setText(msg.getValue());
                    break;
            }

        }


        return convertView;
    }

    /**
     * 根据数据源的position返回需要显示的的layout的type
     * <p/>
     * type的值必须从0开始
     */
    @Override
    public int getItemViewType(int position) {

        ChatListBean msg = myList.get(position);
        int type = msg.getType();
        return type;
    }

    /**
     * 返回所有的layout的数量
     */
    @Override
    public int getViewTypeCount() {
        return 7;
    }

    // 左边的文本
    class ViewHolderLeftText {
        private TextView btnLeftText;
    }

    // 右边的文本
    class ViewHolderRightText {
        private TextView btnRightText;
    }
}
