package com.okl.okl_voice.entity;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   NewsBean
 *  创建者:   LGL
 *  创建时间:  2016/7/5 18:25
 *  描述：    新闻实体类
 */
public class NewsBean {
    /**
     * title : 发改委:择机实施降息降准政策
     * url : http://finance.sina.cn/china/gncj/2016-08-03/detail-ifxunyxy6406419.d.html?vt=1&pos=1
     * id : aa34eca838afbf1701343527a03b69fbc783f44f562413d94a003234d83c4eea
     */

    private String title;
    private String url;
    private String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
