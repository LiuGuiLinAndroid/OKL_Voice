package com.okl.okl_voice.entity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   StaticClass
 *  创建者:   LGL
 *  创建时间:  2016/7/6 9:33
 *  描述：    静态常量合集
 */
public class StaticClass {

    /*----------Debug-------------*/
    public static final boolean isDeBug = true;
    //是否保存pcm录音文件
    public static final boolean isPcm = false;
    //导航使用哪家？
    //百度
    public static final boolean isBaidu = false;
    //高德
    public static final boolean isAmap = true;
    //兴趣点
    public static final boolean isSugg = false;

    //版本区分，现在是用不上的，以后可能用得上，如果用上了，这句话请删掉
    //车机
    public static final boolean isCar = true;
    //后视镜
    public static final boolean isSofia = true;

    /*----------Url-------------*/
    //在线升级json请求的地址
    public static final String UPDATE_URL = "";
    //内网
    public static final String Within_NET = "http://192.168.3.192:4000";
    //外网
    public static final String OUTSIDE_NET = "http://univoicesrv002.51asr.com:4000";

    //天气接口
    public static final String WEATHER_URL = "http://stock.51asr.com:8080/weather?city=";
    //新闻接口
    public static final String NEWS_URL = "http://news.51asr.com:8080/news";
    //股票接口
    public static final String STOCK_URL = "http://stock.51asr.com:8080/stock";

    //高德地图的临时key，需要自己去认证,这个key是暂时的，有风险
    public static final String AMAP_KEY = "389880a06e3f893ea46036f030c94700";

    //笑话接口
    public static final String JOKE_URL = "http://japi.juhe.cn/joke/content/list.from?key=56e5f85c150ebd54461ae4fb7c6705ec&page=2&pagesize=1&sort=asc&time=1418745237";

    /*----------Package-------------*/
    //蓝牙电话本包名
    public static final String BLUETOOTH_PACKAGE = "com.AnywheeBt";
    //蓝牙电话本启动Activity
    public static final String BLUETOOTH_PACKAGE_FIRST = "com.AnywheeBt.MainActivity";
    //蓝牙电话本电话广播
    public static final String BLUETOOTH_CALL = "android.action.intent.bluecall";
    //关闭应用广播
    public static final String STOP_APP = "android.intent.action.STATUSBAR_CLOSEAPP";
    //隐式意图View
    public static final String INTENT_VIEW = "android.intent.action.VIEW";
    //高德地图包名
    public static final String MAP_PACKAGE = "com.autonavi.minimap";
    //酷我音乐的包名
    public static final String KUWO_MUSIC_PACKAGE = "cn.kuwo.kwmusiccar";

    //button窗口的包名路径
    public static final String WINDOW_BUTTON_PACKAGE = "com.okl.okl_voice.service.WindowButtonService";


    /*----------SharedPreferences-------------*/
    //窗口按钮
    public static final String IS_WINDOW_BUTTON = "isWindowButton";
    //windowbutton显示的位置
    public static final String LAST_X = "last_x";
    public static final String LAST_Y = "last_y";
    //唤醒词按钮状态
    public static final String WAKE_UP_STATUS = "wake_up_status";
    //TTS的速度
    public static final String SPEAK_SPEED = "speak_speed";
    //监听home
    public static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    public static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

    /*----------Handler-------------*/
    //保活权限
    public static final int GRAY_SERVICE_ID = 1009;
    //声波纹数据传递
    public static final int AUDIO_LINE_HANDLER = 1111;
    //TTS开始
    public static final int TTS_START_SPEAK = 1001;
    //TTS结束
    public static final int TTS_STOP_SPEAK = 1002;

    //识别玩结束listview
    public static final int CLEAR_LIST_VIEW = 1003;
    //声音停止，开始识别
    public static final int UNHANDLER_STOP_RECORD = 1100;
    //声音喋喋不休
    public static final int UNHANDLER_HAS_BEN_RECORD = 1101;

    //天气的传值
    public static final int RESULT_WEATHER = 1102;

    //左边添加
    public static final int ADD_LIST_LEFT = 1103;
    //右边添加
    public static final int ADD_LIST_RIGHT = 1104;

    //继续录音
    public static final int CONTIUNETO = 1105;

    //返回主頁
    public static final int GO_HOME = 1106;

    //开机修改时间
    public static final int REBOOT_SYSTEM_CLOCK = 1107;

    //显示系统错误的时间window
    public static final int SHOW_TIME_ERROR_WINDOW = 1108;

    //不说话清楚View
    public static final int CLEAR_LIST_VIEW_HANDLER = 1109;


    /*----------Intent-------------*/
    public static final String ACTIVITY_FLAG = "activity_flag";


    /*----------interface-------------*/
    //tts播报广播
    public static final String TTS_INTERFACE = "com.okl.okl_voice_tts_interface";


    /*----------Action-------------*/
    public static final String FM_ACTION_BROADCAST = "com.unisound.unicar.broadcast.action";
    //来电广播
    public static final String ANSWER_ACTION_BROADCAST = "android.intent.action.PHONE_STATE";
    //更改唤醒词
    public static final String NEW_WAKEUP_ACTION_BROADCAST = "com.okl.new_wakeup";
    //来电广播
    public static final String BLUETOOTH_CALL_IN = "BluetoothHeadsetClient.ACTION_CALL_CHANGED";
    public static final String BLUETOOTH_CALL_IN_ACTION = "android.bluetooth.headsetclient.extra.CALL";

    //测试log广播
    public static final String TEST_LOG = "com.okl.okl_voice_log_interface";


}