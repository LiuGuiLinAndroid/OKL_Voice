package com.okl.okl_voice.entity;

import java.util.List;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   WeatherBean
 *  创建者:   LGL
 *  创建时间:  2016/8/10 9:33
 *  描述：    天气类实体
 */
public class WeatherBean {

    /**
     * resultData : [{"cityNameList":[""],"dateTimeList":["今天"],"provinceNameList":[""]}]
     * resultText : 正在为您查询!
     * resultType : Weather_Query
     */

    private String resultText;
    private String resultType;
    private List<ResultDataBean> resultData;

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public List<ResultDataBean> getResultData() {
        return resultData;
    }

    public void setResultData(List<ResultDataBean> resultData) {
        this.resultData = resultData;
    }

    public static class ResultDataBean {
        private List<String> cityNameList;
        private List<String> dateTimeList;
        private List<String> provinceNameList;

        public List<String> getCityNameList() {
            return cityNameList;
        }

        public void setCityNameList(List<String> cityNameList) {
            this.cityNameList = cityNameList;
        }

        public List<String> getDateTimeList() {
            return dateTimeList;
        }

        public void setDateTimeList(List<String> dateTimeList) {
            this.dateTimeList = dateTimeList;
        }

        public List<String> getProvinceNameList() {
            return provinceNameList;
        }

        public void setProvinceNameList(List<String> provinceNameList) {
            this.provinceNameList = provinceNameList;
        }
    }
}
