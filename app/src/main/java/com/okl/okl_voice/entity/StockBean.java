package com.okl.okl_voice.entity;

import java.util.List;

/**
 * Created by Administrator on 2016/7/28.
 */
public class StockBean {
    /**
     * resultData : [{"stockCode":"","stockName":"建设银行"}]
     * resultText : 正在为你您查找！
     * resultType : Stock_Query
     */

    private String resultText;
    private String resultType;
    /**
     * stockCode :
     * stockName : 建设银行
     */

    private List<ResultDataBean> resultData;

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public List<ResultDataBean> getResultData() {
        return resultData;
    }

    public void setResultData(List<ResultDataBean> resultData) {
        this.resultData = resultData;
    }

    public static class ResultDataBean {
        private String stockCode;
        private String stockName;

        public String getStockCode() {
            return stockCode;
        }

        public void setStockCode(String stockCode) {
            this.stockCode = stockCode;
        }

        public String getStockName() {
            return stockName;
        }

        public void setStockName(String stockName) {
            this.stockName = stockName;
        }
    }
}
