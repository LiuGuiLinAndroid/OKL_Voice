package com.okl.okl_voice.entity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   ChatListBean
 *  创建者:   LGL
 *  创建时间:  2016/7/5 18:25
 *  描述：    对话列表实体类
 */
public class ChatListBean {

    //标记
    private int type;
    // 文本
    private String value;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ChatListBean{" +
                "type=" + type +
                ", value='" + value + '\'' +
                '}';
    }
}
