package com.okl.okl_voice.entity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   UserEvent
 *  创建者:   LGL
 *  创建时间:  2016/8/11 12:43
 *  描述：    EventBus实体类
 */

public class UserEvent {

    //标记
    private int type;
    //传递天气1
    private String weather;
    //传递股票2
    private String stock;
    //传递导航3
    private String navigation;
    //传递新闻4
    private String news;
    //错误0
    private String error;


    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getNavigation() {
        return navigation;
    }

    public void setNavigation(String navigation) {
        this.navigation = navigation;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }


}
