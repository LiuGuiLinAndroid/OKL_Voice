package com.okl.okl_voice.entity;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.entity
 *  文件名:   PoiSuggBean
 *  创建者:   LGL
 *  创建时间:  2016/8/29 14:04
 *  描述：    一多个地点的bean */

public class PoiSuggBean {

    //地点名字
    private String name;
    //经纬度
    private double la;
    private double lo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLa() {
        return la;
    }

    public void setLa(double la) {
        this.la = la;
    }

    public double getLo() {
        return lo;
    }

    public void setLo(double lo) {
        this.lo = lo;
    }
}
