package com.okl.okl_voice.receiver;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.receiver
 *  文件名:   SoftwareStateReceiver
 *  创建者:   LGL
 *  创建时间:  2016/8/19 17:18
 *  描述：    监听软件状态
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.utils.TTSUtils;

public class SoftwareStateReceiver extends BroadcastReceiver {

    //TTS
    private TTSUtils mTTs;

    @Override
    public void onReceive(Context context, Intent intent) {
        mTTs = TTSUtils.getInstance(context, null);
        //判断广播类型
        String action = intent.getAction();
        //获取包名
        Uri appName = intent.getData();
        L.i("appName:" + appName);
        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            L.i("安装" + appName);
            try {
                mTTs.startRead("应用安装成功");
            } catch (Exception e) {
            }
        } else if (Intent.ACTION_PACKAGE_REPLACED.equals(action)) {
            //获取属性
            L.i("更新" + appName);
            try {
                mTTs.startRead("应用更新成功");
            } catch (Exception e) {
            }
        } else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)) {
            L.i("卸载" + appName);
            try {
                //应用卸载之后删除
                mTTs.startRead("应用卸载成功");
            } catch (Exception e) {
            }
        }
    }
}
