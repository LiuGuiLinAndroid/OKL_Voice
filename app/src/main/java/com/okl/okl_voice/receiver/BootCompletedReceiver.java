package com.okl.okl_voice.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.okl.okl_voice.R;
import com.okl.okl_voice.service.WakeUpService;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.utils.SharePreUtils;
import com.okl.okl_voice.utils.TTSUtils;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.receiver
 *  文件名:   BootCompletedReceiver
 *  创建者:   LGL
 *  创建时间:  2016/7/5 15:19
 *  描述：    开机自启广播
 */
public class BootCompletedReceiver extends BroadcastReceiver {

    //语音
    private TTSUtils mTTs;

    @Override
    public void onReceive(Context context, Intent intent) {
        L.i("开机广播");
        String name = SharePreUtils.getString(context, "wakeup_name", context.getString(R.string.hello_cube));
        mTTs = TTSUtils.getInstance(context, null);
        mTTs.startRead(context.getString(R.string.reboot_text) + name);
        //启动唤醒服务
        context.startService(new Intent(context, WakeUpService.class));
    }
}
