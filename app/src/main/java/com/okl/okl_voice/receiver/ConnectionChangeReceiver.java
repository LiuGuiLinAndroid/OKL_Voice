package com.okl.okl_voice.receiver;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.receiver
 *  文件名:   ConnectionChangeReceiver
 *  创建者:   LGL
 *  创建时间:  2016/8/4 16:59
 *  描述：    监听网络状态
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import com.okl.okl_voice.R;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.view.CustomDialog;

public class ConnectionChangeReceiver extends BroadcastReceiver implements View.OnClickListener{

    private static CustomDialog dialog_network_status;
    //前往设置，退出
    private Button btnGoIntent,btnFinish;
    //上下文
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
            //网络不可用就dialog
            showDialog(context);
            FunctionUtils.sendTTSBro(context,"你的网络好像出现了点问题！");
        }
    }

    /**
     * 显示网络不可用的提示框
     * @param context
     */
    private void showDialog(Context context) {
        dialog_network_status = new CustomDialog(context, 0, 0, R.layout.dialog_network_error,
                R.style.Theme_dialog, Gravity.CENTER, 0);
        btnGoIntent = (Button) dialog_network_status.findViewById(R.id.btnGoIntent);
        btnGoIntent.setOnClickListener(this);
        btnFinish = (Button) dialog_network_status.findViewById(R.id.btnFinish);
        btnFinish.setOnClickListener(this);
        //屏幕外不可点击
        dialog_network_status.setCancelable(false);
        dialog_network_status.show();
    }

    /**
     * 点击事件
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            //前往设置
            case R.id.btnGoIntent:
                FunctionUtils.toSettingNetWork(mContext);
                dialog_network_status.dismiss();
                break;
            //退出
            case R.id.btnFinish:
                dialog_network_status.dismiss();
                //返回主页
                FunctionUtils.goHome(mContext);
                break;
        }
    }
}
