package com.okl.okl_voice.receiver;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.receiver
 *  文件名:   OutsideInterface
 *  创建者:   LGL
 *  创建时间:  2016/8/17 17:34
 *  描述：    语音对外提供的一些接口
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.TTSUtils;

public class OutsideInterface extends BroadcastReceiver {

    private TTSUtils mTTs;

    @Override
    public void onReceive(Context context, Intent intent) {
        mTTs = TTSUtils.getInstance(context, null);
        String action = intent.getAction();
        //tts播放
        if (action.equals(StaticClass.TTS_INTERFACE)) {
            String tts = intent.getStringExtra("tts_content");
            if(!TextUtils.isEmpty(tts)){
                mTTs.startRead(tts);
            }
        }
    }
}
