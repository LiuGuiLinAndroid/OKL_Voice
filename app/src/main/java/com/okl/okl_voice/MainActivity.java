package com.okl.okl_voice;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.okl.okl_voice.activity.AllFunctionActivity;
import com.okl.okl_voice.activity.MainSettingActivity;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.receiver.ConnectionChangeReceiver;
import com.okl.okl_voice.service.WakeUpService;
import com.okl.okl_voice.utils.FunctionUtils;
import com.okl.okl_voice.utils.L;
import com.okl.okl_voice.utils.SharePreUtils;
import com.okl.okl_voice.utils.TTSUtils;
import com.okl.okl_voice.view.CustomDialog;

import java.util.ArrayList;
import java.util.List;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.activity
 *  文件名:   MainActivity
 *  创建者:   LGL
 *  创建时间:  2016/7/5 14:42
 *  描述：    上海声翰语音SDK集成项目
 *                         _by 刘桂林
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //选项卡
    private ViewPager mViewpager;
    //两个View
    private View view1, view2;
    //容器
    private List<View> mList = new ArrayList<>();
    //小圆点
    private ImageView point1, point2;
    //入口
    private LinearLayout ll_main_call, ll_main_navigation, ll_main_music, ll_main_local, ll_main_all_function, ll_main_setting;
    //唤醒词
    private TextView main_hello_title;
    //更改唤醒词提示框
    private CustomDialog dialog_wake_up;
    //修改笔
    private ImageView iv_edit_wakeupword;
    //输入框
    private EditText et_name;
    //提示来按钮
    private Button btnOk, btnOn;
    //启动窗口
    private ImageView startSpeak;
    //网络状态监听
    private ConnectionChangeReceiver myReceiver;

    private Handler mainHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case StaticClass.GO_HOME:

                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //启动唤醒服务
        startService(new Intent(this, WakeUpService.class));
        registerReceiver();
        initView();
    }

    /**
     * 初始化View
     */
    private void initView() {

        startSpeak = (ImageView) findViewById(R.id.startSpeak);
        startSpeak.setOnClickListener(this);

        main_hello_title = (TextView) findViewById(R.id.main_hello_title);

        String name = SharePreUtils.getString(this, "wakeup_name", getString(R.string.hello_cube));
        L.i("唤醒词：" + name);
        //添加下划线
        main_hello_title.setText(Html.fromHtml("<u>" + name + "</u>"));
        main_hello_title.setOnClickListener(this);

        iv_edit_wakeupword = (ImageView) findViewById(R.id.iv_edit_wakeupword);
        iv_edit_wakeupword.setOnClickListener(this);

        point1 = (ImageView) findViewById(R.id.point1);
        point1.setBackgroundResource(R.drawable.icon_dot_selected);
        point2 = (ImageView) findViewById(R.id.point2);
        point2.setBackgroundResource(R.drawable.icon_dot_normal);

        mViewpager = (ViewPager) findViewById(R.id.mViewpager);

        view1 = View.inflate(this, R.layout.layout_main_one, null);
        ll_main_call = (LinearLayout) view1.findViewById(R.id.ll_main_call);
        ll_main_call.setOnClickListener(this);
        ll_main_navigation = (LinearLayout) view1.findViewById(R.id.ll_main_navigation);
        ll_main_navigation.setOnClickListener(this);
        ll_main_music = (LinearLayout) view1.findViewById(R.id.ll_main_music);
        ll_main_music.setOnClickListener(this);
        ll_main_local = (LinearLayout) view1.findViewById(R.id.ll_main_local);
        ll_main_local.setOnClickListener(this);

        view2 = View.inflate(this, R.layout.layout_main_two, null);
        ll_main_all_function = (LinearLayout) view2.findViewById(R.id.ll_main_all_function);
        ll_main_all_function.setOnClickListener(this);
        ll_main_setting = (LinearLayout) view2.findViewById(R.id.ll_main_setting);
        ll_main_setting.setOnClickListener(this);

        mList.add(view1);
        mList.add(view2);

        //设置数据源
        mViewpager.setAdapter(new MainPagerAdapter());

        //监听事件
        mViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            //切换时状态变换
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    point1.setBackgroundResource(R.drawable.icon_dot_selected);
                    point2.setBackgroundResource(R.drawable.icon_dot_normal);
                } else if (position == 1) {
                    point1.setBackgroundResource(R.drawable.icon_dot_normal);
                    point2.setBackgroundResource(R.drawable.icon_dot_selected);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //电话
            case R.id.ll_main_call:
                Toast.makeText(this, "测试环境:-拨打10086", Toast.LENGTH_SHORT).show();
                FunctionUtils.CallPhone(this, "10086");
                break;
            //导航
            case R.id.ll_main_navigation:
                Toast.makeText(this, "测试环境:导航-天安门", Toast.LENGTH_SHORT).show();
                try {
                    FunctionUtils.StartNavigation(this, 39.9086920000, 116.3974770000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            //音乐
            case R.id.ll_main_music:
                Toast.makeText(this, "测试环境-刘德华的忘情水", Toast.LENGTH_SHORT).show();
                FunctionUtils.PlayMusic(this, "忘情水", "刘德华", "");
                break;
            //周边查询
            case R.id.ll_main_local:
                TTSUtils.getInstance(this,null).startRead("你好，我是小优");
                break;
            //全部功能
            case R.id.ll_main_all_function:
                startActivity(new Intent(this, AllFunctionActivity.class));
                break;
            //设置
            case R.id.ll_main_setting:
                startActivity(new Intent(this, MainSettingActivity.class));
                break;
            //唤醒词
            case R.id.main_hello_title:
                showWakeUpNameDialog();
                break;
            //画笔
            case R.id.iv_edit_wakeupword:
                showWakeUpNameDialog();
                break;
            case R.id.btnOk:
                //获取新的唤醒词
                String newWakeup = et_name.getText().toString();
                if (!TextUtils.isEmpty(newWakeup)) {
                    //更新唤醒词，通过广播
                    Intent intent = new Intent("com.okl.new_wakeup");
                    intent.putExtra("new_wakeup", newWakeup);
                    sendBroadcast(intent);
                    //设置新唤醒词
                    main_hello_title.setText(Html.fromHtml("<u>" + newWakeup + "</u>"));
                    //提示框消失
                    dialog_wake_up.dismiss();
                }
                break;
            case R.id.btnOn:
                dialog_wake_up.dismiss();
                break;
            case R.id.startSpeak:
                //传值
                Intent regIntent = new Intent(this, WakeUpService.class);
                regIntent.putExtra("button_start", "button_start");
                startService(regIntent);
                break;
        }
    }

    /**
     * 显示更改唤醒词提示框
     */
    private void showWakeUpNameDialog() {
        dialog_wake_up = new CustomDialog(this, 0, 0, R.layout.dialog_wake_up_name,
                R.style.Theme_dialog, Gravity.CENTER, 0);
        //设置屏幕外触摸无响应
        dialog_wake_up.setCancelable(false);
        btnOk = (Button) dialog_wake_up.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        btnOk.setEnabled(false);
        btnOn = (Button) dialog_wake_up.findViewById(R.id.btnOn);
        btnOn.setOnClickListener(this);
        et_name = (EditText) dialog_wake_up.findViewById(R.id.et_name);
        //监听
        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    btnOk.setEnabled(true);
                } else {
                    btnOk.setEnabled(false);
                }
            }
        });
        dialog_wake_up.show();
    }

    /**
     * 数据源
     */
    class MainPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView(mList.get(position));
            super.destroyItem(container, position, object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ((ViewPager) container).addView(mList.get(position));
            return mList.get(position);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        L.i("MainActivity onPause");
    }

    /**
     * onStop
     */
    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        L.i("MainActivity onDestroy");

        //关闭网络状态监听
        unregisterReceiver(myReceiver);
        SharePreUtils.putString(this, "wakeup_name", main_hello_title.getText().toString());
    }

    /**
     * onResume
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    //监听按钮
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            //返回键
            case KeyEvent.KEYCODE_BACK:
                FunctionUtils.goHome(this);
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 注册网络状态变化广播
     */
    private void registerReceiver() {
        IntentFilter filter = new IntentFilter(
                ConnectivityManager.CONNECTIVITY_ACTION);
        myReceiver = new ConnectionChangeReceiver();
        this.registerReceiver(myReceiver, filter);
    }



}
