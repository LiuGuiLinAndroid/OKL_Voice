package com.okl.okl_voice.application;

/*
 *  项目名：  OKL_Voice 
 *  包名：    com.okl.okl_voice.application
 *  文件名:   VoiceApplication
 *  创建者:   LGL
 *  创建时间:  2016/8/8 11:26
 *  描述：    Application类
 */

import android.app.Application;

import com.baidu.mapapi.SDKInitializer;
import com.okl.okl_voice.utils.L;
import com.tencent.bugly.crashreport.CrashReport;

public class VoiceApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        L.i("VoiceApplication onCreate");
        //初始化百度地图
        SDKInitializer.initialize(getApplicationContext());
        //初始化腾讯Bugly
        CrashReport.initCrashReport(getApplicationContext(), "900046132", true);
    }
}
