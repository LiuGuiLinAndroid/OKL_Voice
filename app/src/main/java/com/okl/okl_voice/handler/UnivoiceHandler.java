package com.okl.okl_voice.handler;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.RegexUtil;
import com.univoice.UniVoiceWakeUp;
import com.univoice.asr.OfflineBNF;
import com.univoice.callback.OnChangeListener;
import com.univoice.signal.WebRtcVad;
import com.univoiceasr.OnASRResultListener;
import com.univoiceasr.OnlineASR;
import com.univoiceasr.UniVoiceOnlineASR;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jhb on 2016/6/22.
 */
public class UnivoiceHandler {

    Context mContext;
    Handler uiHandler;
    List<short[]> pcm_dataList = new ArrayList<>();
    FileOutputStream fos, fos1, fos_All;
    DataOutputStream mDos, mDos1, mDos_All;

    private OnASRResultListener mAsrResultListener = null;
    private int waitAsrResultTimeout = 15;//设置等待asr回答的超时时间
    private String requestUrl = "";//请求的接口URL
    private int requestTimeout = 30;//发送请求的超时时间
    private long speakEndTime = 2500;//静音多少毫秒后判定讲话结束
    private long sleepTime = 10000;//静音多少毫秒后进入睡眠
    private long forceStopTime = 7000;//连续讲话多少毫秒以后强制结束
    private int frameCount = 20;


    public void setWaitAsrResultTimeout(int waitAsrResultTimeout) {
        this.waitAsrResultTimeout = waitAsrResultTimeout;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }

    public void setSpeakEndTime(long speakEndTime) {
        this.speakEndTime = speakEndTime;
    }

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    public void setForceStopTime(long forceStopTime) {
        this.forceStopTime = forceStopTime;
    }

     UniVoiceWakeUp wakeUp;
    public static UniVoiceOnlineASR onlineASR;

    public boolean isWakeUp = false;
    public boolean isOver = true;//判断当前语音识别的流程是否走完
    boolean isRecording;//判断当前是否正在录制有效声音
    long lastVoice = 0;//记录最后一次说完话的时间
    long firstVoice = 0;//记录开始说话的时间点

    public UnivoiceHandler(Context context, OnASRResultListener asrResultListener, Handler uiHandler) {
        mContext = context;
        mAsrResultListener = asrResultListener;
        this.uiHandler = uiHandler;

        if (StaticClass.isPcm) {
            File file = new File("/sdcard/wakeup/wakeup" + System.currentTimeMillis() + ".pcm");
            File allFile = new File("/sdcard/pcmAll/all" + System.currentTimeMillis() + ".pcm");
            try {
                fos = new FileOutputStream(file);
                mDos = new DataOutputStream(fos);
                fos_All = new FileOutputStream(allFile);
                mDos_All = new DataOutputStream(fos_All);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public void init() {
        wakeUp = new UniVoiceWakeUp(mContext);
        wakeUp.setWakeUpConfidenceThres(10f);
        onlineASR = new UniVoiceOnlineASR(requestUrl, requestTimeout, waitAsrResultTimeout, mAsrResultListener, getMacAddress(mContext));
        OfflineBNF.initOfflineBNF(mContext);
    }

    public void handleVoice(short[] pcm_data, boolean isOfflineBNF) {
        if (pcm_data == null || pcm_data.length == 0) {
            return;
        }
        if (StaticClass.isPcm) {
            if (mDos_All != null)
                SaveSpeechFileAll(pcm_data);
        }
        if (pcm_dataList.size() == frameCount) {
            pcm_dataList.remove(0);
        }
        pcm_dataList.add(pcm_data);
        if (wakeUp == null) return;
        int voiceState = wakeUp.checkState(pcm_data);
        //System.out.println("voiceState:"+voiceState);
        switch (voiceState) {
            case 1:
                for (int i = 0; i < pcm_dataList.size(); i++) {
                    if (StaticClass.isPcm) {
                        if (mDos != null)
                            SaveSpeechFile(pcm_dataList.get(i));
                    }
                    if (!isWakeUp) {
                        String resultStr = wakeUp.getWakeupWord(pcm_dataList.get(i));
                        if (resultStr.trim().equals(wakeUp.wakeUpWord)) {
                            isWakeUp = true;
                            lastVoice = System.currentTimeMillis();
                            //System.out.println("wake up!");
                            mAsrResultListener.ResultHandler("wake up!");
                            frameCount = 45;
                            WebRtcVad.reset();
                            WebRtcVad.setThres((float) 0.1, (float) 0.02, frameCount);
                        }
                    } else {
                        if (isOver) {
                            if (!isRecording) {
                                //uiHandler.sendEmptyMessage(Constant.MENU_DIALOG_SHOW);
                                isRecording = true;
                                if (!isOfflineBNF)
                                    OnlineASR.asrInit();
                                //System.out.println("recording!");
                                firstVoice = System.currentTimeMillis();
                                if (StaticClass.isPcm) {
                                    String fileName = firstVoice + ".pcm";
                                    File file1 = new File("/sdcard/map/asr" + fileName);
                                    try {
                                        fos1 = new FileOutputStream(file1);
                                        mDos1 = new DataOutputStream(fos1);
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            if (isRecording) {
                                lastVoice = System.currentTimeMillis();
                                if (isOfflineBNF) {
                                    OfflineBNF.processPCM(pcm_dataList.get(i));
                                    if ((lastVoice - firstVoice) / 1000 > 2) {
                                        isRecording = false;
                                        isOver = false;
                                        mAsrResultListener.ResultHandler(RegexUtil.getChineseWord(OfflineBNF.getResult()));
                                        //uiHandler.sendEmptyMessage(Constant.MENU_DIALOG_DISMISS);
                                    }
                                } else {
                                    if (StaticClass.isPcm) {
                                        SaveSpeechFile1(pcm_dataList.get(i));
                                    }
                                    onlineASR.processAsr(pcm_dataList.get(i));
                                    if (lastVoice - firstVoice > forceStopTime) {
                                        isRecording = false;
                                        isOver = false;
                                        //如果是，则说明讲话结束
                                        onlineASR.stopAsr();
                                        //setVoiceSleep();
                                        //System.out.println("speak stop!");
                                        //uiHandler.sendEmptyMessage(Constant.MENU_DIALOG_DISMISS);
                                    }
                                }
                            }
                        }
                    }
                }
                pcm_dataList.clear();
                break;
            case 0:
                if (isWakeUp) {
                    if (isOver && isRecording) {
                        for (int i = 0; i < pcm_dataList.size(); i++) {
                            onlineASR.processAsr(pcm_dataList.get(i));
                        }
                        pcm_dataList.clear();
                        long difTime = System.currentTimeMillis() - lastVoice;
                        //如果是，则说明讲话结束
                        if (difTime >= speakEndTime) {
                            isRecording = false;
                            isOver = false;
                            lastVoice = System.currentTimeMillis();
                            if (isOfflineBNF) {
                                mAsrResultListener.ResultHandler(RegexUtil.getChineseWord(OfflineBNF.getResult()));
                            } else {
                                onlineASR.stopAsr();
                            }
                            //uiHandler.sendEmptyMessage(Constant.MENU_DIALOG_DISMISS);
                            frameCount = 45;
                            WebRtcVad.reset();
                            WebRtcVad.setThres((float) 0.3, (float) 0.1, frameCount);
                        }
                    }

                    if (System.currentTimeMillis() - lastVoice >= sleepTime && isOver && !isRecording) {
                        setVoiceSleep();
                        //当没有声音的时候，通知window取消
                        uiHandler.sendEmptyMessage(StaticClass.CLEAR_LIST_VIEW_HANDLER);
                    }
                } else {
                    for (int i = 0; i < pcm_dataList.size(); i++) {
                        String resultStr = wakeUp.getWakeupWord(pcm_dataList.get(i));
                        if (resultStr.trim().equals(wakeUp.wakeUpWord)) {
                            isWakeUp = true;
                            lastVoice = System.currentTimeMillis();
                            //System.out.println("wake up!");
                            mAsrResultListener.ResultHandler("wake up!");
                            frameCount = 45;
                            WebRtcVad.reset();
                            WebRtcVad.setThres((float) 0.1, (float) 0.02, frameCount);
                        }
                    }
                    pcm_dataList.clear();
                }
                break;
            case -1:
                break;
        }


    }

    public void setVoiceSleep() {
        isWakeUp = false;
        isRecording = false;
        isOver = true;
        System.out.println("sleep!");
        frameCount = 20;
        WebRtcVad.reset();
        WebRtcVad.setThres((float) 0.1, (float) 0.02, frameCount);
    }

    public void changeNewWakeUpWord(String newWord, OnChangeListener onChangeListener){
        wakeUp.downloadWakeUpWordBin(newWord,onChangeListener);
    }

    public int reLoadWakeUpResource(){
        return wakeUp.reLoadWakeUpResource();
    }

    public void setWakeUpConfidenceThres(float thres){
        wakeUp.setWakeUpConfidenceThres(thres);
    }

    void SaveSpeechFile(short[] pcm) {
        ByteBuffer bb = ByteBuffer.allocate(pcm.length * 2);
        try {
            for (short s : pcm) {
                byte[] le_bytes = new byte[2];
                le_bytes[0] = (byte) (s & 0xff);
                le_bytes[1] = (byte) (s >> 8);
                bb.put(le_bytes);
            }
            mDos.write(bb.array(), 0, pcm.length * 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void SaveSpeechFile1(short[] pcm) {
        ByteBuffer bb = ByteBuffer.allocate(pcm.length * 2);
        try {
            for (short s : pcm) {
                byte[] le_bytes = new byte[2];
                le_bytes[0] = (byte) (s & 0xff);
                le_bytes[1] = (byte) (s >> 8);
                bb.put(le_bytes);
            }
            mDos1.write(bb.array(), 0, pcm.length * 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void SaveSpeechFileAll(short[] pcm) {
        ByteBuffer bb = ByteBuffer.allocate(pcm.length * 2);
        try {
            for (short s : pcm) {
                byte[] le_bytes = new byte[2];
                le_bytes[0] = (byte) (s & 0xff);
                le_bytes[1] = (byte) (s >> 8);
                bb.put(le_bytes);
            }
            mDos_All.write(bb.array(), 0, pcm.length * 2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseSpeechFile() {
        try {
            mDos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseSpeechFile1() {
        try {
            mDos1.close();
            fos1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseSpeechFileAll() {
        try {
            mDos_All.close();
            fos_All.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取手机mac地址<br/>
     * 错误返回12个0
     */
    public static String getMacAddress(Context context) {
        // 获取mac地址：车机 0a0bac3e238d;手机 f48e9270cfbb
        String macAddress = "000000000000";
        try {
            WifiManager wifiMgr = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = (null == wifiMgr ? null : wifiMgr
                    .getConnectionInfo());
            if (null != info) {
                if (!TextUtils.isEmpty(info.getMacAddress())) {
                    macAddress = info.getMacAddress().replace(":", "");
                    Log.i("Mac", macAddress);
                } else
                    return macAddress;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return macAddress;
        }
        return macAddress;
    }

}
