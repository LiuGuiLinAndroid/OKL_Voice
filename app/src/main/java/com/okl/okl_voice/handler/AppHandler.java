package com.okl.okl_voice.handler;

import org.json.JSONException;
import org.json.JSONObject;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   AppHandler
 *  创建者:   LGL
 *  创建时间:  2016/7/21 15:07
 *  描述：    打开应用封装
 */
public class AppHandler {

    public String doLogic(String result){
        String [] data = null;
        String type = "";
        JSONObject json = null;
        if (result.contains("打开")){
            data = result.split("打开");
            type = "Open_App";
        }else if (result.contains("启动")){
            data = result.split("启动");
            type = "Open_App";
        }else if (result.contains("关闭")){
            data = result.split("关闭");
            type = "Close_App";
        }
        if (data!=null&&data.length>0){
            try {
                json = new JSONObject();
                json.put("resultType",type);
                json.put("appName",data[data.length-1]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (json!=null)
            return json.toString();
        else
            return null;
    }
}
