package com.okl.okl_voice.handler;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   SettingHandler
 *  创建者:   LGL
 *  创建时间:  2016/8/11 15:07
 *  描述：    快捷控制封装
 */
public class SettingHandler {

    public SettingHandler(){
    }

    public void init(){
    }

    public String doLogic(String result){
        String type = "";
        //打开/关闭WIFI，打开/关闭蓝牙，调低/调高亮度
        switch (result){
            case "增加音量":
            case "提高音量":
            case "调高音量":
            case "增大音量":
                type = "Volume_Raise";
                break;
            case "减小音量":
            case "降低音量":
            case "调低音量":
                type = "Volume_Lower";
                break;
            case "最大音量":
            case "调到最大音量":
            case "静音":
                type = "Volume_Max";
                break;
            case "最小音量":
            case "调到最小音量":
                type = "Volume_Min";
                break;
            case "打开WIFI":
            case "打开无线网":
            case "打开网线网络":
                type = "WIFI_Open";
                break;
            case "关闭WIFI":
            case "关闭无线网":
            case "打开关闭网络":
                type = "WIFI_Close";
                break;
            case "打开蓝牙":
                type = "Bluetooth_Open";
                break;
            case "关闭蓝牙":
                type = "Bluetooth_Close";
                break;
            case "调低亮度":
                type = "Light_Lower";
                break;
            case "调高亮度":
                type = "Light_Raise";
                break;
        }
        if (TextUtils.isEmpty(type)){
            return null;
        }else{
            JSONObject json = new JSONObject();
            try {
                json.put("resultType","Setting");
                json.put("className",type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json.toString();
        }
    }

    public void destroy(){
    }
}
