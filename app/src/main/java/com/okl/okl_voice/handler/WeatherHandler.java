package com.okl.okl_voice.handler;

import android.util.Log;

import com.google.gson.Gson;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.entity.WeatherBean;
import com.okl.okl_voice.utils.OKhttpManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   WeatherHandler
 *  创建者:   LGL
 *  创建时间:  2016/7/21 15:07
 *  描述：    天气解析封装
 */
public class WeatherHandler {
    Gson mGson;
    OKhttpManager mOKhttpManager = OKhttpManager.getInstance();
    int mDateIndex;//日期下标 0:今天,1：明天,2:后天,3:今天明天,4:明天后天,5:今天明天后天

    String mCityName, mDateTime;

    public WeatherHandler(Gson gson) {
        this.mGson = gson;
    }

    public void init() {

    }

    public int doFirstChoose(String json, OKhttpManager.ResultCallback mWeatherQueryListener) {
        WeatherBean weatherBean = mGson.fromJson(json, WeatherBean.class);
        List<String> cityNameList = weatherBean.getResultData().get(0).getCityNameList();
        List<String> dataTimeList = weatherBean.getResultData().get(0).getDateTimeList();
        if (cityNameList.size() == 0 && dataTimeList.size() == 0) {
            return -1;
        }
        String city = "";
        String dateTime = "";
        mDateIndex = 0;
        if (cityNameList.size() > 0) city = cityNameList.get(0);
        if (city.equals("")) {
            //默认是深圳
            city = "深圳";
        }
        if (dataTimeList.size() > 0) {
            for (int i = 0; i < dataTimeList.size(); i++) {
                dateTime += dataTimeList.get(i);
            }
        }
        mCityName = city;
        mDateTime = dateTime;

        if (dateTime.equals("今天")) {
            mDateIndex = 0;
            dateTime = "0";
        } else if (dateTime.contains("后天") || dateTime.contains("3天")) {
            if (dateTime.equals("后天")) {
                mDateIndex = 2;
            } else if (dateTime.equals("明天后天")) {
                mDateIndex = 4;
            } else if (dateTime.equals("今天明天后天") || dateTime.contains("3天")) {
                mDateIndex = 5;
            }
            dateTime = "2";
        } else if (dateTime.contains("明天") || dateTime.contains("2天")) {
            if (dateTime.equals("明天")) {
                mDateIndex = 1;
            } else if (dateTime.equals("今天明天") || dateTime.contains("2天")) {
                mDateIndex = 3;
            }
            dateTime = "1";
        } else {
            mDateIndex = 0;
            dateTime = "0";
        }

        mOKhttpManager._getAsyn(
                StaticClass.WEATHER_URL + city + "&day=" + dateTime, mWeatherQueryListener
        );

        return 1;
    }

    public String handleWeatherResponse(String response) throws JSONException {
        JSONArray array = new JSONArray(response);
        JSONObject first = null;
        JSONObject second = null;
        JSONObject third = null;
        switch (mDateIndex) {
            case 0:
                first = array.getJSONObject(0);
                break;
            case 1:
                second = array.getJSONObject(1);
                break;
            case 2:
                third = array.getJSONObject(2);
                break;
            case 3:
                first = array.getJSONObject(0);
                second = array.getJSONObject(1);
                break;
            case 4:
                second = array.getJSONObject(1);
                third = array.getJSONObject(2);
                break;
            case 5:
                first = array.getJSONObject(0);
                second = array.getJSONObject(1);
                third = array.getJSONObject(2);
                break;
            default:
                first = array.getJSONObject(0);
                break;

        }
        JSONArray temp = new JSONArray();
        if (first != null) {
            temp.put(first);
        }
        if (second != null) {
            temp.put(second);
        }
        if (third != null) {
            temp.put(third);
        }
        JSONObject data = new JSONObject();
        data.put("resultData", temp);
        data.put("dateTime", mDateTime);
        data.put("cityName", mCityName);
        Log.i("xxx", data.toString());
        return data.toString();
    }

    public void destroy() {
        mGson = null;
    }
}
