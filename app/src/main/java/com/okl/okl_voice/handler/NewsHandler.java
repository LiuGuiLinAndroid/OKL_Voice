package com.okl.okl_voice.handler;


import com.okl.okl_voice.entity.NewsBean;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.utils.OKhttpManager;

import java.util.List;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   NewsHandler
 *  创建者:   LGL
 *  创建时间:  2016/7/21 15:07
 *  描述：    新闻解析封装
 */
public class NewsHandler {

    OKhttpManager mOKhttpManager = OKhttpManager.getInstance();

    public boolean isSearchNews(String result){
        boolean flag = false;
        if (result.contains("新闻")){
            flag = true;
        }
        return flag;
    }

    public void doFirstChoose(OKhttpManager.ResultCallback<List<NewsBean>> mNewsQueryListener){
        mOKhttpManager._getAsyn(StaticClass.NEWS_URL, mNewsQueryListener);
    }

}
