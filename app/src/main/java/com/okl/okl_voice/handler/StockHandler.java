package com.okl.okl_voice.handler;

import com.google.gson.Gson;
import com.okl.okl_voice.entity.StaticClass;
import com.okl.okl_voice.entity.StockBean;
import com.okl.okl_voice.utils.OKhttpManager;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.handler
 *  文件名:   StockHandler
 *  创建者:   LGL
 *  创建时间:  2016/7/21 15:07
 *  描述：    股票解析封装
 */
public class StockHandler {
    OKhttpManager mOKhttpManager = OKhttpManager.getInstance();
    Gson mGson;

    public StockHandler(Gson mGson) {
        this.mGson = mGson;
    }

    public int doFirstChoose(String json, OKhttpManager.ResultCallback mStockQueryListener) {
        StockBean stock = mGson.fromJson(json, StockBean.class);
        StockBean.ResultDataBean stockData = stock.getResultData().get(0);
        if (stockData.getStockName().equals("")) {
            return -1;
        }
        mOKhttpManager._getAsyn(StaticClass.STOCK_URL + "?name=" + stockData.getStockName(), mStockQueryListener);
        return 1;
    }

    public String[] handleStockResponse(String response) {
        String content = response.split("=")[1];
        if (content.length() <= "\"\";".length()) {
            return null;
        }
        String data = content.substring(1, content.length() - 2);
        String[] dataArray = data.split(",");
        return dataArray;
    }
}
