/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : Telephony.java
 * @ProjectName : Tool
 * @PakageName : cn.yunzhisheng.phone
 * @Author : Brant
 * @CreateDate : 2014-6-21
 */
package com.okl.okl_voice.phone;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

/*
 *  项目名：  OKL_Voice
 *  包名：    com.okl.okl_voice.service
 *  文件名:   Telephony
 *  创建者:   LGL
 *  创建时间:  2016/8/8 17:35
 *  描述：    语音服务
 */
public class Telephony {

	/**
	 * 接听
	 * @param context
     */
	public static void answerRingingCall(Context context) {
		// Make sure the phone is still ringing
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if (tm.getCallState() != TelephonyManager.CALL_STATE_RINGING) {
			return;
		}
		// Answer the phone
		try {
			answerPhoneAIDL(context);
		} catch (Exception e) {
			e.printStackTrace();
			answerPhoneHeadsetHook(context);
		}
	}

	public static ITelephony getITelephony(Context context) {
		TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		Class c = TelephonyManager.class;
		Method getITelephonyMethod = null;
		try {
			// 获取声明的方法
			getITelephonyMethod = c.getDeclaredMethod("getITelephony", (Class[]) null);
			getITelephonyMethod.setAccessible(true);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		try {
			// 获取实例
			return (ITelephony) getITelephonyMethod.invoke(mTelephonyManager, (Object[]) null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void answerPhoneHeadsetHook(Context context) {
		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		try {
			context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Intent headSetUnPluggedintent = new Intent(Intent.ACTION_HEADSET_PLUG);
		headSetUnPluggedintent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY);
		headSetUnPluggedintent.putExtra("state", 1); // 0 = unplugged 1 =
														// Headset with
														// microphone 2 =
														// Headset without
														// microphone
		headSetUnPluggedintent.putExtra("name", "Headset");
		// TODO: Should we require a permission?
		try {
			context.sendOrderedBroadcast(headSetUnPluggedintent, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void silenceRinger(Context context) throws RemoteException {
		ITelephony telephonyService = getITelephony(context);
		// Silence the ringer and answer the call!
		telephonyService.silenceRinger();
	}

	public static void answerPhoneAIDL(Context context) throws RemoteException {
		ITelephony telephonyService = getITelephony(context);
		telephonyService.answerRingingCall();
	}

	/**
	 * 挂断电话
	 * @param context
     */
	public static void endCall(Context context) {
		ITelephony telephonyService = getITelephony(context);
		// Silence the ringer and answer the call!
		try {
			telephonyService.endCall();
		} catch (RemoteException e) {
			e.printStackTrace();
			answerPhoneHeadsetHook(context);
		}
	}

	/**
	 * 判断当前是否在通话
	 * @param context
	 * @return
     */
	public static boolean phoneIsInUse(Context context) { 
	     boolean phoneInUse = false; 
	     try { 
	    	 ITelephony phone = getITelephony(context); 
	    	 if (phone != null){
	    		 phoneInUse = !phone.isIdle();
	    	 }
	    } catch (Exception e) { 
	     }
	     return phoneInUse; 
	 }
}
