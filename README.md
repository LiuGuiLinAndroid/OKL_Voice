
# 欧克蓝语音
---
>Create in 2016 on August 4

- 网络框架：RxVolley：https://github.com/kymjs/RxVolley

>用于软件更新下载

- Json解析：Gson:https://github.com/google/gson

>语音返回结果转换json

- 百度地图：http://lbsyun.baidu.com/

>正反编码/定位

- 网络框架：OkHttp:http://square.github.io/okhttp/

>查询天气

- 事件总栈：EventBus3.0:https://github.com/greenrobot/EventBus

>子线程更新UI

- 高德地图：http://lbs.amap.com/

---

>更新日志：

- 1.完善UI界面
- 2.UI界面交互逻辑添加
- 3.新增语音声波View(有内存损耗，后期应该移除)
- 4.等待接入语音SDK


>出差进度

## 2016/08/08 周一： 

- 1.新增五个aar
- 2.新增UnvoiceHandler录音类
- 3.对相关资源的初始化，熟悉语音流程
- 4.反馈问题：aar有点大，2MB的应用加入aar后41MB
- 5.微调TTS（暂未通过）result:-1
- 6.新增腾讯Bugly，捕捉异常Crash

```java	
	bugList:https://bugly.qq.com/v2/crash/apps/900046132?pid=1
```
>Bugly debug模式开启，请在发布时把isDebug关闭

- 7.TTS调通（声音通道 | 网络 ）
- 8.封装TTSUtils，使用方法
	
```java
	//TTS
    private TTSUtils mTTs;
	//初始化TTS工具类
	mTTs = TTSUtils.getInstance(Context);
	//开始朗诵
	mTTs.startRead(text);
	//停止
	mTTs.stopRead();
	//销毁
	mTTs.destoryTTs();
```

- 9.新增语音识别
	
>isRecording只作为标识	

	
```java
 	/**
     * 开启录音识别
     */
    private void startRec() {
        mAudioProcessThread.start_rec();
        isRecording = true;
    }

    /**
     * 关闭录音识别
     */
    private void stopRec() {
        mAudioProcessThread.stop_rec();
        isRecording = false;
    }
```

- 10.正在调通语音解析相关部分功能

```java
	 //初始化录音识别
      initAudioRecord();
     //接收回调信息
      initUnivoiceHandler();
```

>初始化完成之后就可以调用识别了，直接startRec后有两个结果

- 1.说话声停止
	
- 2.说话声喋喋不休，我们人为控制秒数关闭（默认8s）
	
```java
	 //人为控制，默认8s
	 if ((lastVoice - firstVoice) / 1000 >= 8) 
```

>两个状态结束都会返回一个handler的what


```java
	//声音停止，开始识别
    public static final int UNHANDLER_STOP_RECORD = 1100;
    //声音喋喋不休
    public static final int UNHANDLER_HAS_BEN_RECORD = 1101;
```

- 11.新增唤醒--WakeUpService

>实现逻辑：全局开启，当识别到wake up的时候唤醒，也就是启动WindowService，然后再开启识别，以此类推，感觉这里应该做一些优化，先按照这套逻辑来，后续优化把！

>这里主要做的事情就是

```java
   //初始化TTS
    mTTs = TTSUtils.getInstance(getApplicationContext());
   //绑定通知栏
    startNotification();
   //初始化录音识别
    initAudioRecord();
   //接收回调信息
    initUnivoiceHandler();
   //初始化windowservice
    windowService=  new WindowService();
```

- 12.新增内网外网

>调试一律用外网，内网属于声翰科技，只作为联调使用

```java
 	//内网
    public static final  String Within_NET = "http://192.168.3.192:4000";
    //外网
    public static final  String OUTSIDE_NET = "http://univoicesrv002.51asr.com:4000";
```

- 13.唤醒初步调通，存在流程问题
- 14.唤醒调通（前台/后台皆可唤醒）
	
	
## 2016/08/09 周二： 

>今天主要是想把基本功能给做好

- 1.等待接入指令解析功能
- 2.删除WindowService，把功能都集成在WakeUpservice中
- 3.初步调通识别，可以接收到指令，逻辑不完善
- 4.新增Gson
- 5.TTSUtils新增Hanlder去得到开始读和结束的回调

```java
	 //TTS开始
    public  static final int TTS_START_SPEAK = 1001;
    //TTS结束
    public  static final int TTS_STOP_SPEAK = 1002;
```

- 6.唤醒————>指令调通（按钮指令后期增加）
- 7.准备新增逻辑操作
- 8.WakeUpService流程


```java
	//初始化各种
	1.init
	//等待回调
	2.OnASRResultListener
	//唤醒
	3.wake up!
	//处理回调
	4.handlerAsrResult
	//处理json
	5.UniVoiceNLU
	//执行操作
	6.resultType

```

- 9.新增拨打电话功能

>电话功能一共有五个type，暂时只开发了一个拨打

```java
	//没有号码
	NO_NUMBER
	//没有联系人
	NO_PERSON
	//多个号码
	MULTIPLE_NUMBERS
	//拨打号码或者姓名，通用
	CONFIRM_CALL
```

- 10.正在调节物理唤醒
- 11.新增歌曲播放

>歌曲功能一共有七个type，目前只开发了一个播放

```java
	//播放歌曲
	Media_PlaySong
	//播放列表我爱听的
	Media_PlayPlaylist
	//播放文件夹我不爱听的
	Media_PlayFolder
	//我最喜欢的歌曲
	Media_PlayFavorites
	//列表播放
	Media_SetRepeatMode
	//上一首
	Media_PlayNext
	//下一首
	Media_PlayPrevious
```

- 12.准备接入定位模块
- 13.新增导航模块

>定位模块一共五个type

```java
	//查找位置，xx省xx市xx区......
	Navi_FindAddress
	//我的位置
	Navi_WhereAmI
	//某个地方的交通情况
	Traffic_Query
	//某个地方/区的地铁或者公交
	Navi_SearchPoiType
	//某个地点附近的地铁或者公交
	Navi_SearchPoiTypeByPos
```

- 14.去除EventBus3.0
- 15.增加百度地图模块辅助导航功能

```java
	//测试Key，正式版的时候重新申请
	Key:ITqkzr87ftGpDAlmCNEDZGK65LDjXLKh
```
- 16.新增定位功能

```java
	//当前的详细地址/处于哪一个地址附近
	L.i("你当前的位置:" + myAddress + "处于：" + location.getLocationDescribe());
```

- 17.新增导航功能


## 2016/08/10 周三：
>今天争取把逻辑交互部分和现有的指令完善

- 1.更改百度地图SDK，修复导航位置编码错误
- 2.更换百度地图Key

```java
	Key：LWkrwAOs9TTWEddFoxanjDGmPmsDuxZT
```
- 3.导航调通
- 4.准备增加物理识别

>暂时还有点逻辑上的问题，先放一边

- 5.新增天气查询
- 6.新增OkHttp&&OKhttpManager

```java
	private OKhttpManager mOKhttpManager = OKhttpManager.getInstance();
```

- 7.新增WeatherHandler,负责天气查询请求反馈

```java
	0:今天
	1:明天
	2:后天,
	3:今天明天
	4:明天后天
	5:今天明天后天
```

- 8.物理按键调通，存在逻辑问题
- 9.WeatherHandler使用方法
	
```java
	//请求解析
	 weatherHandler.doFirstChoose（json,ResultCallback<String>）；
	//结果
	String json = weatherHandler.handleWeatherResponse(response);
	//得到的Json
	
	{
    "cityName": "上海",
    "dateTime": "今天",
    "resultData": [
        {
            "d1": "东南风",
            "d2": "东风",
            "f1": "zhenyu",
            "f2": "duoyun",
            "p1": "≤3",
            "p2": "≤3",
            "s1": "阵雨",
            "s2": "多云",
            "t1": "34",
            "t2": "28"
        }
    ]
	}

	//字段的含义
	s1:白天天气
	s2:夜间天气
	f1:白天天气拼音
	f2:夜间天气拼音
	t1:最高温度
	t2:最低温度
	p1:白天风力
	p2:夜间风力
	d1:白天风向
	d2:夜间风向

	//通过长度去判断他要的天数，最多支持三天
```

- 10.更改反馈字段
- 11.天气调通
- 12.新增StockHandler

```java
	//使用方法和Weather一样，返回的数组，自己拿下标

	0：”大秦铁路”，股票名字；
	1：”27.55″，今日开盘价；
	2：”27.25″，昨日收盘价；
	3：”26.91″，当前价格；
	4：”27.55″，今日最高价；
	5：”26.20″，今日最低价；
	6：”26.91″，竞买价，即“买一”报价；
	7：”26.92″，竞卖价，即“卖一”报价；
	8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
	9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
	10：”4695″，“买一”申请4695股，即47手；
	11：”26.91″，“买一”报价；
	12：”57590″，“买二”
	13：”26.90″，“买二”
	14：”14700″，“买三”
	15：”26.89″，“买三”	
	16：”14300″，“买四”
	17：”26.88″，“买四”
	18：”15100″，“买五”
	19：”26.87″，“买五”
	20：”3100″，“卖一”申报3100股，即31手；
	21：”26.92″，“卖一”报价
	(22, 23), (24, 25), (26,27), (28, 29)分别为“卖二”至“卖四的情况”
	30：”2008-01-11″，日期；
	31：”15:05:32″，时间；
```

- 13.股票调通
- 14.准备接入新闻
- 15.新闻调通

>新闻是比较简陋的，暂时，只要需要判断是否有新闻两个字就开始去解析了

```java
	//判断是否是新闻
	newsHandler.isSearchNews(result)
	//开始解析，返回的是list，直接可以装载adapter
	newsHandler.doFirstChoose
```

- 16.准备接入POI搜索当地美食/小吃
- 17.POI检索，不完善
- 18.新增语速调节


```java
	 //TTS的速度
    public static final String SPEAK_SPEED = "speak_speed";
```

>明天对一下还有遗留的问题

- TTS -1
- 基本功能 打开/关闭 应用 对话
- 更改唤醒词


## 2016/08/11 周四：

>今天把基本功能完善，同时把一些遗留的问题商讨

- 1.更改百度地图Key:

```java
	Key：33C8e7cvQ7H9ZkziDF7sfr8BMgUiViMd
```

- 2.新增AppHandler
- 3.准备接入打开/关闭应用模块

```java
	 //是否是打开/关闭应用
        String appJson = appHandler.doLogic(result);
        if (!TextUtils.isEmpty(appJson)) {
            L.i("appJson:" + appJson);
        }
```

- 4.打开/关闭应用模块调通
- 5.导航调通
- 6.新增EventBus3.0

```java
	//根据type去区分动作
	0：error
	1:更新天气
	2:更新股票
```

- 7.探讨的结果：

```java
	1.TTS进入-1是因为资源占用，rm -rf 云之声
	2.唤醒词不可软件层更改，定制一个唤醒词更改一个架包
```
- 8.天气调通
- 9.股票调通
- 10.新增SettingHandler快捷控制

>这个要注意的一点就是要在打开APP前面使用，避免被判断成打开应用

- 10.准备接入快捷控制
- 11.快捷控制接入成功
- 12.新增打开/关闭蓝牙
- 13.新增增大/减小/最大/最小音量
- 14.新增打开/关闭wifi

>写到这里，其实我自己是有一点担忧的，因为整个程序对识别度很是依赖，缺少了了算法规避，所以去判断关键字的时候会出现一些难识别的现象

- 15.百度地图的正反编码有点坑
- 16.更改为高德地图的api接口


```java
	API:http://restapi.amap.com/v3/geocode/geo?key=xx&s=rsv3&city=35&address=xx
```
- 17.新增高德地图
- 18.申请高德地图key

```java
	//高德地图的key
    public static final St	ring AMAP_KEY = "d048c004423d6a83497633edfabff8b4";
```

- 19.选用临时key

```java
	//高德地图的key
    public static final String AMAP_KEY = "389880a06e3f893ea46036f030c94700";
```
- 20.导航调通

>明天更改唤醒词：你好，小优！
	
## 2016/08/12 周五：

>今天把功能都完善了

- 1.大部分的逻辑都改善了一下
- 2.修复部分卡死状态

>关于识别度低的问题，确实有这个现象，因为比较生硬，我可以先尝试的把pcm录音文件给保存下来，然后看下有没有问题

- 3.新增测试环境，保存pcm到内存卡
- 4.新增自定义词汇
- 5.新增当前时间
- 6.新增新闻窗口

>采用的是Service上Windows的方式，所以可能就没有不能使用Dialog了，那么就都是使用Window+Window的方式了，这样的话，返回的措施就得做好了

 - 7.联调识别率
 - 8.识别率问题更改完成

>解答一下今天的几个Bug

- 1.语音识别率低，识别的字会出错，还有就是说一串比较长的语音，前面几个字和后面几个字就没有了。
- 2.没有模糊识别，如"拨打电话"，识别成"把电话"，这个时候其实也应该模糊匹配到拨打电话的，而这个却没有。
- 3.UI交互基本方面，一是命令执行错误，如导航到世界之窗，导航到湖北去了。播放视频，又进入到酷我音乐去了。二是，语音没有退出之前，还需要叫你好魔方来进行对话，三就是一些交互方面也不是很友好，请参照云之声的一些做法。总的来说交互方面还是DEMO阶段。

>第一和第二个联调完成，声翰方面说下周更新一版更改参数的sdk
>第三，关于UI方面，我暂时可能做的可能没有那么的细致，但是基本功能，其实都是可以的，命令错误，是声翰这边反馈的动作识别可能需要完善一下，导航到湖北，是之前使用的百度地图去搜索导航，百度地图默认是深圳的世界之窗，但是百度不是很稳定，所以我更改成了高德的，他没有默认值，只会去全国搜索，所以导致只说世界之窗的话，会先搜到湖北的世界之窗而导航到湖北那边去了

## 2016/08/15 周一：
>今明两天把功能点都核对完

- 1.新增百度/高德导航切换
- 2.更新百度地图key
>应该正式打包的，不然debug前面麻烦

```java
	key:2MC7a2ONFSqjdoB2LX7xBSbsyyPBpXrg
```
- 3.导航更新完成：导航到世界之窗——》深圳世界之窗
- 4.交互方面
- 5.关闭广播无效
- 6.物理按键需要调整
- 7.功能都对一下
```java
	1.TTS -1（在后视镜-1，分析原因）
	2.对话功能/更多的动作
	3.反馈不清晰——关闭蓝牙|一来呀    （播放音乐/播放视频）/命令错误 人大一样/上一首
	4.唤醒词：你好小优
	5.仍然存在录音有声音反馈null的情况
	6.识别不准（存在发音还是？）
	7.比较生硬：（打开蓝牙）比如说：开蓝牙，就识别不出了
	8.距离——0.5m error
	9.世界之窗附近的小吃
```

- 8.回复邮件
- 9.准备更改唤醒词：你好小优
- 10.唤醒词更改成功

## 2016/08/16 周二：

- 1.更新唤醒词小优的返回词（更新SDK）
- 2.更新百度地图key：
```java
	Key:ZlGrsBGbcM4KRcdosq3T5ztmb9GUr7aE
```

- 3.新增友好提示，tts播报完后咚的一声
- 4.新增两种播放模式


## 2016/08/17 周三：

- 1.新增home监听
- 2.新增引导语句
- 3.新增新闻
- 4.新增新闻页面
- 5.新闻页面完善
- 6.没有听清增加双录音
>后续全部都要加上

- 7.你好加上双录音
- 8.准备完善各个录音
- 9.对话功能都加上了双录音
- 10.版本更新1.0.7
- 11.新增对外接口

```java
	//tts播报广播
    public static final String TTS_INTERFACE = "com.okl.okl_voice_tts_interface";
	//使用方法发送广播，传值put
	name =  "tts_content"

	//实例
	Intent intent = new Intent("com.okl.okl_voice_tts_interface");
	intent.putExtra("tts_content", "我是tts的内容");
	sendBroadcast(intent);
```

- 12.今天的问题

```java
	1.说：你好语音助理，也可以唤醒
	2.识别不准，识别率差，错别字
	3.丢头
	4.动作不全
	5.开始识别 你不说话 仍然识别出东西来
	6.tts变调，"你好啊，你还可以继续说"，这个说字
```

## 2016/08/18 周四：
>逻辑啊逻辑

- 1.新增拨号确定或取消
- 2.新增亮度调节
- 3.新增新闻加载进度
- 4.完善逻辑

>困惑：物理按键录音为什么会一直报子线程更新UI的错误呢？

>识别率仍然是一个很大的问题
>TTS也需要优化


## 2016/08/19 周五：

- 1.新增字体滚动
- 2.录音波纹修改
- 3.取消字体滚动
- 4.新增返回键控制window

>到这里，还遗留着两个问题需要解决

- 1.物理按键
- 2.UI绘制卡

>新发现的几个问题

- 导航：北京的地铁，返回的却是导航

- 5.更新的wakeUp sdk，希望能解决唤醒词：语音助理的问题
- 6.版本更新1.0.9
- 7.新增软件状态监听，当有更新的时候操作

## 2016/08/25 周四：
>今天声翰更新了SDK，但是好多逻辑其实都被他打乱了

```java
1.更新内容：
(1)univoicenlusdk.aar;
初始化：UnivoiceNluUtils mUnivoiceNluUtils = new UnivoiceNluUtils(getApplicationContext());
使用方法：String json = mUnivoiceNluUtils.processResult(result);
返回Json类型详解:
设置类：{"resultType":"Setting","className":"Volume_Raise"}

视频类：{"resultType":"Media_PlayVideo","videoName":"天下无贼"} 

新闻类：{"resultType":"NEWS_QUERY","originalText":"查询一下今天的新闻"} 
*originalText：你说的原话


(2)对UnivoiceHandler类,UnivoiceWakeUp.aar进行了优化;
优化了识别不准，掉字等情况。

(3)UnivoiceTTS.aar更新；（该.aar已通过QQ先行发送给刘工了）
解决了个别字词的变音，以及TTS返回-1的问题。

2.注意事项
改版本变动的内容的相对不是很多，如有不清

```

>现在开始一个个去调试

- 1.修改TTS，修复-1的问题，貌似还是有
- 2.更改语音逻辑
- 3.大部分功能协调成功


## 2016/08/26 周五：
>今天把尾巴都完善了

1.增加了物理逻辑
2.大多数逻辑调通
3.无明显bug

>唯一注意的是：

- 1.多次子线程更新的问题
- 2.tts介绍滴的一下有时候不响会直接导致二次逻辑


>还有什么没有做？

- 1.音量最大最小
- 2.亮度增大增小
- 3.搜索结果多样化（poi）
- 4.多个号码电话


>版本更新自1.1.1（给测试测试）


## 2016/08/29 周一：

>测试反馈的问题进行了一遍梳理

- 自身的问题：

```java
 	1.二次逻辑 
	2.tts 结束状态未接收到
	3.语音识别错误时，不会退出
	4.天气界面
	5.调频98.5
	6.poi检索
```
- SDK的问题

```java
 	1.难唤醒
	2.误唤醒
	3.TTS结束监听偶尔接收不到
	4.tts声音不好听
	5.说简洁语句识别不了，比如‘我要吃饭’，或者‘吃饭’，‘去玩’都识别不了
	6.识别率低
	7.没有FM：（调频98.5）条坪98.5
	8.没有聊天功能
	9.希望可以更改唤醒词（自定义）
```

- 1.尝试修改二次逻辑
- 2.逻辑梳理有问题 去掉二次逻辑
- 3.去除通知栏绑定
- 4.新增Poi多个地点，正在调通
- 5.poi多个地点已经调通
- 6.多次叠加逻辑有问题，已解决，暂未找到完美解决方案
- 7.开始整理调频
- 8.发现TTS不出声时间问题正在更改


## 2016/08/30 周二：

>准备修复TTS的问题，现有的逻辑是开机之后去更正时间

- 1.打包签名，密码为：okl123456
- 2.更新正式版百度地图的key
- 3.key测试统购
- 4.系统时间暂时不处理
- 5.准备更新天气显示


## 2016/09/05 周一：

>今天把新的SDK集成，然后二次逻辑，发布

- 1.版本更新versionName "1.1.4"
- 2.更新wakeupsdk
- 3.更新UnivoiceHandler
- 4.新增可更改唤醒词
- 5.优化代码逻辑


## 2016/09/06 周二：
>逐步完善

- 1.新增FM调频
- 2.新增电话监听，接听/挂断


## 2016/09/07 周四：

- 1.修复一些bug
- 2.新增笑话功能
- 3.新增来电提示
- 4.版本更新1.1.6


## 2016/09/21 周三：
>期间一直在做长安讯飞语音,本次更新较多

- 1.版本更新1.1.8
- 2.修复旧逻辑所带来的carsh
- 3.新增本地识别
- 4.优化天气显示
- 5.新增二次讲话逻辑
- 6.新增导航多选
- 7.新增新闻多选
- 8.新增歌曲识别上一首-下一首-单曲循环之类
- 9.新增调频FM（后视镜版本）
- 10.更多更新细节等待测试

>遗留下来的问题，当二次讲话错误的时候，下次进入讲依旧二次逻辑，只识别场景

```java
 	//导航
    private boolean isNavigationTwo = false;
    //新闻
    private boolean isNewsTwo = false;
    //联系人
    private boolean isContactTwo = false;
    //继续读
    private boolean isSpeakTwo = false;
```

>二次变量会一直增加

>遗留的SDK问题

- 1.开机TTS杂音
- 2.首次唤醒难唤醒
- 3.首次识别难识别



...


## 2016/10/11 周三：
- 修复逻辑bug
- 修复播报天气，股份后窗口还在的bug
- 新增地点列表数字排序